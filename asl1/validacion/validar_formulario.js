	$(document).ready(function() {
		//Validacion con BootstrapValidator
	    $('#form1')
	        .bootstrapValidator({
	            message: 'Este valor no es válido',
	            //fields: name de los inputs del formulario, la regla que debe cumplir y el mensaje que mostrara si no cumple la regla
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            fields: {
	            	cedu: {
                        message: 'La cédula no es valido',
                        validators: {
                            notEmpty: {
                                message: 'La cédula es requerido y no puede estar vacío',
                            },
                            stringLength: {
                                min: 7,
                                max: 8,
                                message: 'Por favor ingrese valor entre 7 y 8 caracteres',
                            },
                            regexp: {
                                regexp: /^[0-9]+$/,
                                message: 'La cédula sólo puede admitir numeros',
                            },
                           	remote: {
		                        type: 'POST',
		                        url: '../validacion/validarcedula.php',
		                        message: 'La cedula ya existe, escriba otro',
		                        delay: 500,
                    		}
                        }
                    },
	                ape: {
	                    message: 'El campo no es valido',
	                    validators: {
	                        notEmpty: {
	                            message: 'El apellido es requerido y no puede estar vacío',
	                        },
	                        stringLength: {
	                            min: 3,
	                            max: 25,
	                            message: 'Por favor ingrese valor entre 3 y 25 caracteres',
	                        },
	                        regexp: {
	                            regexp: /^[A-Z][a-zñáóíé]+$/,
	                            message: 'El apellido sólo puede tener caracteres alfabéticos',
	                        }
	                    }
	                },
	                nom: {
	                    message: 'El campo no es valido',
	                    validators: {
	                        notEmpty: {
	                            message: 'El nombre es requerido y no puede estar vacío',
	                        },
	                        stringLength: {
	                            min: 3,
	                            max: 25,
	                            message: 'Por favor ingrese valor entre 3 y 25 caracteres',
	                        },
	                        regexp: {
	                            regexp: /^[A-Z][a-zñáóíé]+$/,
	                            message: 'El nombre sólo puede tener caracteres alfabéticos',
	                        }
	                    }
	                },
	                sex: {
	                	message: 'El campo no es valido',
               			 validators: {
                    		notEmpty: {
                        		message: 'El genero es requerido y no puede estar vacío'
                    		}
               			}
           			},
           			tipos: {
		                validators: {
		                    notEmpty: {
		                        message: 'El código es requerido y no puede estar vacío'
		                    }
		                }
		            },
		            tele: {
						 message: 'El teléfono no es valido',
						 validators: {
							 notEmpty: {
								 message: 'El teléfono es requerido y no puede ser vacío'
							 },
							 regexp: {
								 regexp: /^[0-9]+$/,
								 message: 'El teléfono solo puede contener números'
							 }
						 }
					 },
					profe: {
		                validators: {
		                    notEmpty: {
		                        message: 'El grado de instrucción es requerido y no puede estar vacío'
		                    }
		                }
		            },
		            par: {
	                	message: 'El campo no es valido',
               			 validators: {
                    		notEmpty: {
                        		message: 'El tipo de participante es requerido y no puede estar vacío'
                    		}
               			}
           			},
           			institu: {
	                    message: 'El campo no es valido',
	                    validators: {
	                        stringLength: {
	                            min: 3,
	                            max: 30,
	                            message: 'Por favor ingrese valor entre 3 y 30 caracteres',
	                        },
	                        regexp: {
	                            regexp: /^[A-Za-zñáóíé\s]+$/,
	                            message: 'El campo sólo puede tener caracteres alfabéticos',
	                        }
	                    }
	                },
           			cor: {
						 validators: {
							 notEmpty: {
								 message: 'El correo es requerido y no puede ser vacío'
							 },
							 emailAddress: {
								 message: 'El correo electrónico no es valido'
							 }
						 }
					 },
		            esta: {
		                validators: {
		                    notEmpty: {
		                        message: 'La provincia es requerida y no puede estar vacío'
		                    }
		                }
		            },
		            usuario: {
	                    message: 'El nombre de usuario no es válido',
	                    validators: {
	                        notEmpty: {
	                            message: 'El nombre de usuario es requerido y no puede estar vacío'
	                        },
	                        stringLength: {
	                            min: 4,
	                            max: 10,
	                            message: 'Por favor ingrese valor entre 4 y 10 caracteres'
	                        },
	                        regexp: {
	                            regexp: /^[a-zA-Zñáóíé]+$/,
	                            message: 'El nombre de usuario sólo puede consistir alfabético'
	                        },
	                        remote: {
		                        type: 'POST',
		                        url: '../validacion/validarusuario.php',
		                        message: 'El usuario no esta disponible, escriba otro',
		                        delay: 500,
                    		}
	                    }
	                },
	                clave: {
		                validators: {
		                    notEmpty: {
		                        message: 'Se requiere la contraseña y no puede estar vacío'
		                    },
		                    stringLength: {
	                            min: 8,
	                            max: 16,
	                            message: 'Por favor ingrese valor entre 8 y 16 caracteres'
	                        },
	                        regexp: {
	                            regexp: /^[A-Za-z0-9ñáóíé@.*,-\/]+$/,
	                            message: 'El campo sólo puede consistir alfabético, numeros y caracteres especiales (*), (,), (-), (/), (@), (.)'
	                        },
		                    identical: {
		                        field: 'vclave',
		                        message: 'La contraseña y su confirmación no son los mismos'
		                    }
		                }
		            },
		            vclave: {
		                validators: {
		                    notEmpty: {
		                        message: 'Se requiere la confirmación de contraseña y no puede estar vacío'
		                    },
		                    stringLength: {
	                            min: 8,
	                            max: 16,
	                            message: 'Por favor ingrese valor entre 8 y 16 caracteres'
	                        },
	                        regexp: {
	                            regexp: /^[A-Za-z0-9ñáóíé@.*,-\/]+$/,
	                            message: 'El campo sólo puede consistir alfabético, numeros y caracteres especiales (*), (,), (-), (/), (@), (.)'
	                        },
		                    identical: {
		                        field: 'clave',
		                        message: 'La contraseña y su confirmación no son los mismos'
		                    }
		                }
		            },
		           	secre: {
	                	message: 'El campo no es valido',
               			 validators: {
                    		notEmpty: {
                        		message: 'La pregunta secreta es requerida y no puede estar vacío'
                    		}
               			}
           			},
	                respues: {
	                    message: 'El campo no es valido',
	                    validators: {
	                        notEmpty: {
	                            message: 'La respuesta secreta es requerida y no puede estar vacío',
	                        },
	                        stringLength: {
	                            min: 3,
	                            max: 25,
	                            message: 'Por favor ingrese valor entre 3 y 25 caracteres',
	                        },
	                        regexp: {
	                            regexp: /^[a-zA-Z0-9ñáóíé\s]+$/,
	                            message: 'El respuesta secreta sólo puede tener caracteres alfabeticos y numeros',
	                        }
	                    }
	                },
	            }
	        })
	        .on('success.form.bv', function(e) {
	            // Prevent form submission
	            e.preventDefault();

	            // Get the form instance
	            var $form = $(e.target);

	            // Get the BootstrapValidator instance
	            var bv = $form.data('bootstrapValidator');

	            // Use Ajax to submit form data
	            $.post($form.attr('action'), $form.serialize(), function(result) {
	                console.log(result);
	            }, 'json');
	        });
	});