	$(document).ready(function() {
	    $('#form1')
	        .bootstrapValidator({
	            message: 'Este valor no es válido',
	            feedbackIcons: {
	                valid: 'glyphicon glyphicon-ok',
	                invalid: 'glyphicon glyphicon-remove',
	                validating: 'glyphicon glyphicon-refresh'
	            },
	            fields: {
	                cedu: {
	                    message: 'El nombre de usuario no es válido',
	                    validators: {
	                        notEmpty: {
	                            message: 'El nombre de usuario es requerido y no puede estar vacio'
	                        },
	                        stringLength: {
	                            min: 5,
	                            max: 20,
	                            message: 'Por favor ingrese valor entre 5 y 20 caracteres'
	                        },
	                        regexp: {
	                            regexp: /^[a-zA-Zñáóíé]+$/,
	                            message: 'El nombre de usuario solo puede tener <br> caracteres alfabeticos',
	                        },
	                    }
	                },
	                clave: {
	                    validators: {
	                        notEmpty: {
	                            message: 'La contraseña es requerida y no puede estar vacio'
	                        },
	                    	stringLength: {
	                            min: 8,
	                            max: 16,
	                            message: 'Por favor ingrese valor entre 8 y 16 caracteres'
	                    	},
	                    	regexp: {
	                            regexp: /^[A-Za-z0-9ñáóíé*.-@.\/]+$/,
	                            message: 'El campo sólo puede consistir alfabético, numeros <br>y caracteres especiales (*), (.), (-), (@), (.)'
	                        },
	               		}
	                },

	            }
	        })
	        .on('success.form.bv', function(e) {
	            // Prevent form submission
	            e.preventDefault();

	            // Get the form instance
	            var $form = $(e.target);

	            // Get the BootstrapValidator instance
	            var bv = $form.data('bootstrapValidator');

	            // Use Ajax to submit form data
	            $.post($form.attr('action'), $form.serialize(), function(result) {
	                console.log(result);
	            }, 'json');
	        });
	});