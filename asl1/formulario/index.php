	 <?php
		require_once("../configuracion/conectarse.conf.php");
		$cedula = @trim($_POST['cedu']);
		$consulta = @mysqli_query($conecto, "SELECT * FROM personas WHERE CedulaPersonas='$cedula';");
		$x = @mysqli_num_rows($consulta);
		if ($x > 0){
	
	?>
		<center>Usted ya se encuentra registrado en el sistema</br>
		<a href="../"> REGRESAR </a> </center>
	<?php	
		} else if ($x == 0){ 
	?>
    <?php   require("../configuracion/funciones.conf.php"); ?> 
	
<html>
	<head>
    
    <link rel="stylesheet" media="screen" type="text/css" href="../estilos/style1.css"/>
 		<title>Formulario de registro</title>
	</head>
	<body>
    		<div id="cabecera" class="cabecera">
           <?=cabecerappl2();?>
        	</div><br><br><br>
                     <div class="letrasdiv1">Debes completar todos los campos obligatorios (marcados con un * ) en el formulario</div>    <br/>
		<form name="form1" action="../formulario/insertar_base_datos.php" method="post" >
       
		<div align="center" class="formulario1"><br/>

        <fieldset class="fieldset">
        <table align="center" class="tt-wrapper">
			<tr>
            	<td colspan="1"><img src="../images/addusers.png" class="img"/></td>
                <td><h1>DATOS PERSONALES</h1>
					<h2>(*) CAMPOS OBLIGATORIOS</h2>
                </td>
            </tr>
            
            <tr>
			 	<td colspan="1"><strong>C�dula:</strong></td>
				<td><input pattern="[0-9]{7,8}" min="1000000" max="100000000" name="cedu" id="cedul"  maxlength="8" required title="El campo debe contener 7 o 8 n�meros dependiendo de los n&uacute;meros que contenga la ced&uacute;la de identidad el mismo solo admite n&uacute;meros, ejemplo: 22080100" value="<?=$cedula?>"  class="input2">*</td>
                <td><a><span><br>El campo debe contener 7 o 8 n�meros dependiendo de los n&uacute;meros que contenga la ced&uacute;la de identidad el mismo solo admite n&uacute;meros, ejemplo: 22080100</span></a></td>
            </tr>

			<tr>
	 			<td colspan="1"><strong>Apellido:</strong></td>
				<td colspan="1"><input pattern="[A-Z]{1}[a-z�����]{3,20}" name="ape" maxlength="20" required title="El campo debe llenarse con la primera letra May&uacute;scula y las dem&aacute;s en min&uacute;sculas, no se aceptan caracteres especiales solo admite letras, ejemplo: Urdaneta." class="input2">*</td>
                <td><a><span>El campo debe llenarse con la primera letra May&uacute;scula y las dem&aacute;s en min&uacute;sculas, no se aceptan caracteres especiales solo admite letras, ejemplo: Urdaneta.</span></a></td>
			</tr>
			
			<tr>
				<td colspan="1"><strong>Nombre:</strong></td>
				<td colspan="1"><input pattern="[A-Z]{1}[a-z�����]{3,20}" name="nom" maxlength="30" required title="El campo debe llenarse con la primera letra May&uacute;scula y las dem&aacute;s en min&uacute;sculas, no se aceptan caracteres especiales solo admite letras, ejemplo: Rosa"  class="input2">*</td>
                <td><a><span>El campo debe llenarse con la primera letra May&uacute;scula y las dem&aacute;s en min&uacute;sculas, no se aceptan caracteres especiales solo admite letras, ejemplo: Rosa.</span></a></td>
			</tr>

			<tr>
				<td colspan="1"><strong>G�nero:</strong></td>
				<td colspan="1">Masculino<input type="radio"  name="sex" required  value="Masculino">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
			            		Femenino<input type="radio"  name="sex" required value="Femenino">*</td>
			</tr>

			<tr>
				<td colspan="1"><strong>Grado de instruci&oacute;n:</strong></td>                
				<td><select name="profe" id="select4" class="input2">
              	    	<option value="0">Seleccione</option> 
			<?php 
						$consultarprofesion = @mysqli_query($conecto, "SELECT * FROM gradoinstruccion;");
						while ($imprimir = mysqli_fetch_array($consultarprofesion)){
			?> 
            			<option value="<?=$imprimir[CodigoGrado];?>"><?=$imprimir[NombreGrado];?></option>
			<?php
						}
			?>			
					</select>
                     </td>
                <td><a><span><br><br><br>Seleccione una de las opciones, esta parte depende del grado de instrucci&oacute;n que tengas</span></a></td>
			</tr>
            
			<tr>
				<td><strong>Tel&eacute;fono:</strong></td>
				<td><select name="tipos" id="select2">
              	    	<option value="0">Seleccione</option> 
			<?php 
						$consultartipos = @mysqli_query($conecto, "SELECT * FROM tipostelefonos;");
						while ($imprimir = mysqli_fetch_array($consultartipos)){
			?> 
            			<option value="<?=$imprimir[CodigoTelefonos];?>"><?=$imprimir[TiposTelefonos];?></option>
			<?php
						}
			?>			
					</select>
              
				<input pattern="[0-9]{4}[-]{1}[0-9]{7}" name="tele" maxlength="13" required title="Por favor llene este campo con 4 n�meros, seguido de un - y luego los 7 n�meros restantes, Ejemplo: 0000-0000000">*</td>
                <td><a><span><br><br>El campo debe llenarse con 4 n�meros, seguido de un gui&oacute;n (-) y luego los 7 n�meros restantes, Ejemplo: 0000-0000000</span></a></td>
			</tr>
            
			<tr>
				<td><strong>Tipo de Participante:</strong></td>
				<td>
                
                <?php
					$participante = mysqli_query ($conecto, "SELECT * FROM tipoparticipantepersonas");
					while ($imprimirparticipante = mysqli_fetch_array($participante))		{	
					if ($imprimirparticipante[TipoParticipante]=="Estudiante" or $imprimirparticipante[CodigoTipoParticipante]=="1"){
						
						?> <input type="radio" name="par" id="radio1" required value="<?=$imprimirparticipante[CodigoTipoParticipante]?>"onClick="document.getElementById('estudiante').hidden=false"><?=$imprimirparticipante[TipoParticipante]?>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<br> <?php
						
					}else {
					?> <input type="radio" name="par" id="radio1" required value="<?=$imprimirparticipante[CodigoTipoParticipante]?>"onClick="document.getElementById('estudiante').hidden=true"><?=$imprimirparticipante[TipoParticipante]?><br> <?php
					}}
				?>
				</td>
			</tr>	
            
			<tr id="estudiante" hidden="true">
                	<td colspan="1"><strong>Instituci&oacute;n:</strong></td>
                    <td><select name="institu" id="select5" class="input2">
              	    	<option value="0">Seleccione</option> 
                    <?php 
						$consultarinstitucion = @mysqli_query($conecto, "SELECT * FROM institucion;");
						while ($imprimir = mysqli_fetch_array($consultarinstitucion)){
			?> 
            			<option value="<?=$imprimir[CodigoInstitucion];?>"><?=$imprimir[NombreInstitucion];?></option>
			<?php
						}
			?>			
            		</select></td>        
            </tr>
            
			<tr>
				<td colspan="1"><strong>Correo Electr�nico:</strong></td>
				<td colspan="1"><input pattern="[\w-\.]{3,}@([\w-]{2,}\.)*([\w-]{2,}\.)[\w-]{2,4}" name="cor" maxlength="50" required title="Por favor llene este campo con letras, seguidas de un @, luego continua con letras, . y letras, Ejemplo:urdaneta@gmail.com"  class="input2">*</td>
		        <td><a><span><br><br>El campo debe contener letras, seguidas de un @, luego continua con letras, un punto(.) y letras, Ejemplo:<br/>urdaneta@gmail.com</span></a></td>
			</tr>
            
			<tr>
				<td><strong>Provincia:</strong></td>
				<td><select name="esta" id="select1" class="input2">
              	    	<option value="0">Seleccione</option> 
			<?php 
						$consultarestados =   @mysqli_query($conecto, "SELECT * FROM provincia;");
						while ($imprimir=mysqli_fetch_array($consultarestados)){
			?> 
            			<option value="<?=$imprimir[EstadoPersonas];?>"><?=$imprimir[NombreEstados];?></option><?php
						}
			?>			
					</select>*</td>
                    <td><a><span><br><br><br>Seleccione una de las opciones, dependiendo a la provincia a la que perteneces</span></a></td>
			</tr>
            </table></fieldset></div>
            
            <div class="arrediv"><fieldset class="fieldset1">
            <table align="center" class="tt-wrapper">
           		<tr>
                	<td><strong>Usuario:</strong></td>
					<td colspan="1"><input pattern="[a-zA-Z]{4,10}[0-9]{0,3}" name="usuario" maxlength="30" required title="El campo puede contener n&uacute;meros o letras de tu preferencia, el m&iacute;nimo de caracteres es 4 y el mm&aacute;ximo es 10, ejemplo: Usuario" class="input2">*</td>
                    <td><a><span><br>El campo puede contener n&uacute;meros o letras de tu preferencia, el m&iacute;nimo de caracteres es 4 y el mm&aacute;ximo es 10, ejemplo: Usuario</span></a></td>
				</tr>
            
           		<tr>
	 				<td colspan="1"><strong>Contrase&ntilde;a:</strong></td>
					<td colspan="1"><input pattern="[a-zA-Z*,-/@.0-9]{8,16}" type="password" name="clave" id="clave" maxlength="30" required title="El campo puede contener n&uacute;meros, letras o los siguientes caracteres especiales(*)&nbsp;(,)&nbsp;(-)&nbsp;(/)&nbsp;<br>(@)&nbsp;(.)&nbsp; el m&iacute;nimo de caracteres es 8 y el m&aacute;ximo es 16 ejemplo: contrasena1*" class="input2">*</td>
                    <td><a><span>El campo puede contener n&uacute;meros, letras o los siguientes caracteres especiales(*)&nbsp;(,)&nbsp;(-)&nbsp;(/)&nbsp;<br>(@)&nbsp;(.)&nbsp; el m&iacute;nimo de caracteres es 8 y el m&aacute;ximo es 16 Ejemplo: contrasena1*</span></a></td>
				</tr>
                
             	<tr>
	 				<td colspan="1"><strong>Verificar Contrase&ntilde;a:</strong></td>
					<td colspan="1"><input pattern="[a-zA-Z*,-/@.0-9]{8,16}" type="password" name="vclave" maxlength="30" required title="El campo debe llevar la misma contrase&ntilde;a que coloco en el campo de arriba ejemplo: contrasena1*" class="input2">*</td>
                    <td><a><span><br><br>El campo debe llevar la misma contrase&ntilde;a que coloco en el campo de arriba ejemplo: contrasena1*</span></a></td>
				</tr>
                
           	<tr>
				<td colspan="1"><strong>Ingrese un comentario:</strong></td>
				<td><textarea name="comentario" maxlength="1000" rows="4" cols="18"  class="input2"></textarea></td>
                 <td><a><span><br><br><br>Este campo puede quedar vac&iacute;o, pero si tienes alg&uacute;n comentario puedes colocarlo aqu&iacute;</span></a></td>
			</tr>
		</table> 
        </br></br>
     
		<center><button type="submit" class="botones2" title="Permite enviar cada uno de los datos que est&aacute;n en el formulario"> Enviar datos</button>
		<button type="reset" class="botones2" onclick=location='./' title="Permite borrar todos los campos, as&iacute; que si te confundiste puedes volver a introducirlos sin problemas">Limpiar datos</button><br>
        </br><button  onclick=location='../' title="Permite regresar a la p&aacute;gina principal" class="botones2"><img  src="../images/atras.png" class="img1"></button>
		</fieldset>
        </form></center>  </div> 
</body>
</html>      
   <?php		
		}
	?>