<!DOCTYPE html>
<?php
session_start();
if ( ! isset($_POST['cedu']))
{ //------------------------------------Formulario de entrada----------------------------------------------------
?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="shortcut icon" type="image/x-icon" href="images/favicon.ico">
	<link rel="shortcut icon" type="image/vnd.microsoft.icon" href="images/favicon.ico">
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/estilos1.css"> 
    <link rel="stylesheet" href="validacion/css/bootstrapValidator.css"/>
	<script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="validacion/js/bootstrapValidator.js"></script>

	<title>Inicio</title>
</head>
<body>
	<div class="container">
	<img class="header1 img-responsive" src="images/header1.png">	
	<br><br>
	
	<div>
		<article>
			<h5 class="letras1">>>>>>BIENVENIDO AL SISTEMA DE POSTULACION ASL-FUNDACITE<<<<<</h5>
		</article>
	</div>
	
		<div class="row">
			<div class="divfondo img-responsive col-xs-12 col-sm-12 col-md-12 col-lg-12">
				<form name="form1" id="form1" method="post" action="#" class="form-inline">
			        <div>
			            <h4 class="letras2">Inicie sesi&oacute;n o reg&iacute;strese para postularse en los cursos de la Academia de Software Libre.</h4>
			        </div>
			        <div>
			            <img src="images/dibujo1.jpg" class="dibujo col-xs-12 col-sm-12 col-md-12 col-lg-12">
			        </div>
			        <div>
			            <fieldset class="col-xs-12 col-sm-6 col-md-6 col-lg-6">

			            	<table  align="center"><br>
			            		<caption class="text-center"><b>Iniciar sesi&oacute;n</b></caption>
			            		<?php
									  if (isset($_SESSION['mensaje']))
									 {
												echo "<span style='color:red; font-size:18pt;'>",$_SESSION['mensaje'],"</span>";
												//echo "<script>alert(",$_SESSION['mensaje'],");</script>";
									  }   

								?>

			            		<tr>
			            			<td>
			            				<div class="form-group input-group input-group-md">
											<span class="input-group-addon" id="sizing-addon1"><img src="images/addusers.png" class="img4"/></span>
				            				<input type="text" name="cedu" value="<?php if (isset($_SESSION['cedu'])) echo $_SESSION['cedu'] ?>" class="form-control" placeholder="Usuario"  maxlength="20" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo debe contener el nombre de usuario que creaste cuando te registraste para poder acceder al sistema"><br>
										</div>
				            		</td>
			            		</tr>
			            		<tr>
			            			<td>
			            				<div class="form-group input-group input-group-md">
			            					<span class="input-group-addon" id="sizing-addon1"><img src="images/candado.png" class="img4"/></span>
			            					<input type="password" name="clave" class="form-control" placeholder="Contraseña" maxlength="20" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo debe contener la contrase&ntilde;a que creaste cuando te registraste para poder tener acceso al sistema" aria-describedby="sizing-addon1" >
			            				</div>
			            			</td>
			            		</tr>
			            		<?php
}
else
{ //-----------------------------------recepcion de los datos del formulario-------------------------------------
   function validar_entradas()
   {  
     if(strlen($_POST['cedu']) == 0)  
            { $_SESSION['mensaje']="Error: Nombre de usuario vac&iacute;o.";
              $_SESSION['NombreUsuario']="";  
              return false; 
            }
     else if(!preg_match("/^[a-zA-Zñáóíé\s]+$/", $_POST['cedu']))  
            { $_SESSION['mensaje']="Error: Nombre del usuario inválido."; 
              return false; 
            } 
     if(strlen($_POST['clave']) == 0)  
            { $_SESSION['mensaje']="Error: Contraseña vac&iacute;a."; 
             return false; 
         	}
     else if(strlen($_POST['clave']) < 8)  
             { $_SESSION['mensaje']="Error: contraseña menor a 8 caracteres."; 
               return false; } 
             else if(!preg_match("/^[A-Za-záéíóúñÑÁÉÍÓÚ0-9]+$/", $_POST['clave']))  
                  { $_SESSION['mensaje']="Error: Contraseña inválida.";     
                    return false; } 
     return true;
   }


   if (validar_entradas()==false)  
   {   
       if ( ! empty($_POST['cedu'])) $_SESSION['NombreUsuario']= $_POST['cedu'];
       echo "<meta content=0 http-equiv=REFRESH> </meta>";  
   }
   else
   {  
       $u=$_POST['cedu'];
       $c=md5($_POST['clave']); 	
       $_SESSION['autenticado']=FALSE;
       require_once("configuracion/conectarse.conf.php");

       $Sentencia_SQL="SELECT * FROM usuarios WHERE NombreUsuario='$u' AND ContrasenaUsuario='$c' ";
       $result = mysqli_query($conecto,$Sentencia_SQL);
       
       if ($REG = mysqli_fetch_array($result))
       { 

	   
	   $_SESSION['autenticado']=TRUE;
	   		$_SESSION['nivel']=$REG[NivelUsuario];
            $_SESSION['nombre']=$REG[NombreUsuario];
		    $_SESSION['person']=$REG[Id_Personas];
           
           header("location:php/menu.php");	 

       }
       else 
       {   $_SESSION['mensaje']="Error, usuario o contrase&ntilde;a incorrecta, intente nuevamente.<br>
           Si ha olvidado alguno, consulte con el administrador del sistema.";
           $_SESSION['NombreUsuario']="";
           echo "<meta content=0 http-equiv=REFRESH> </meta>";
       }
   }  
}
?>
			            		<tr>
			            			<td align="center"><br>

                                        <input type="submit" class="btn btn-danger active botones3" value="Iniciar sesi&oacute;n" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Permite iniciar sesi&oacute;n colocando los datos correctos" value="Ingresar">			            			<br><br>
			            			</td>
			            		</tr>
			   
               
               
               
               
               
               
               
                </form>
			            		<tr>
			            			<td align="center"><b><a href="#modal1" title="¿Has olvidado su contrase&ntilde;a?" data-toggle="modal" class="text-danger text-left">¿Has olvidado su contrase&ntilde;a?</a></b><hr></td>
			            			<!--Ventana emergente recuperar contraseña-->
			            			<div class="modal fade" id="modal1"><br><br><br>
										<div class="modal-dialog">
											<div class="modal-content">
												<!--Header de la ventana-->
												<div class="modal-header">
													<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
													<h4 class="modal-title" align="center">¿Has olvidado su contrase&ntilde;a?</h4>
												</div>
												<!--Contenido de la ventana-->
												<div class="modal-body">
													<div class="">
														<label class="control-label col-md-2">Correo electronico:</label>
														<div class="col-md-3 col-md-offset-3">
                                            				<input type="text" name="email" class="form-control" maxlength="50" placeholder="Correo electronico" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Debes colocar el correo electronico que esta registrado en el sistema">
                                       					</div> 
													</div>
												</div>
												<!--Footer de la ventana-->
												<div class="modal-footer">
													<button type="submit" class="btn btn-primary" onclick=location='php/olvidastes_contrasena.php'>Enviar correo</button>
													<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
												</div>
											</div>
										</div>
									</div>
			            		</tr>

			            		<tr>
			            			<td align="center">
			            				
                                      <a href="php/registrarse.php">  <div class="btn btn-danger active botones3"  rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Permite ir al formulario de registro">
                                        Registrarse </div></a> <br><br>
			            			</td>
			            		</tr>
			            	</table>
			            </fieldset>
			        </div>
			    
		    </div>
		</div>
	<footer>
		<div class="container-fluid">
			<p class="text-danger"><b>@ 2015</b></p>
			<a href="registro/creditos.php" class="text-success">CREDITOS</a>
		</div>
	</footer>
	</div>
	
	<script>
		$('[rel="popover"]').popover({
			trigger:'hover', //click hover focus manual
			html: true,
			delay: 100,
		})
	</script>
</body>
</html>
