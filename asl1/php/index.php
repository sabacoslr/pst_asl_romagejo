<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0 ">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
		<link rel="stylesheet" href="../css/estilos.css">
		<title>Cursos</title>
	</head>
	<body class="container">
		<?php
			/*Conexión con la base de datos*/
			require_once("../configuracion/conectarse.conf.php");
			/*Diferentes funciones que tiene el sistema*/
			require_once("../configuracion/funciones.conf.php");
			/*Para que la sesión este activada*/
			@session_start();
		?>

		<!-- Banner del sistema-->
		<img class="img-responsive" alt="Imagen responsive" src="../images/header1.png">	
		<!--Fin del Banner-->

		<header>
			<nav class="navbar navbar-default" role="navigation">	
			<div class="container">
				<!--Botón que se coloca cuando las pantallas son mas pequeñas-->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion">
						<span class="sr-only">Desplegar / ocultar menu</span>
						<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
				</div>
				<!--Fin del bótón-->

	    		<!-- Inicia Menu-->
	    		<div class="collapse navbar-collapse" id="navegacion">
				    	<ul class="nav navbar-nav">
				        	<li class="active"><a href="#">Inicio</a></li>

				        	<li>
				         		<a href="" class="dropdown-toggle" data-toggle="dropdown">Cursos <span class="caret"></span></a>
								<ul class="dropdown-menu aria-labelledby="dLabel"" role="menu" >
									<li><a href="cursos.php">Consultar cursos</a></li>
								</ul>
				         	</li>

				         	<li class="dropdown">
				         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Postulantes <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Visualizar postulantes</a></li>
								</ul>
				         	</li>

				         	<li class="dropdown">
				         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Reportes estadísticos <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Reporte de participante por curso</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Reporte por género</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Reporte por tipo de participante</a></li>
									<li role="separator" class="divider"></li>
								</ul>
				         	</li>

					        <li><a href="#">Ayuda</a></li>

					        <li class="dropdown">
					        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Configuración
					        	<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Perfil</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Niveles de usuarios</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Características generales de otras instituciones</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Salir</a></li>
									<li role="separator" class="divider"></li>
								</ul>
					        </li>
					   </ul>
	  			</div>
	  			<!--Fin del menú -->
	  			</div>
				</nav>
		</header>
			
		<?php if($_GET[control]==""){

		}

			/*ADMINISTRADORES*/
			$obtener2=$_GET['control'];
		?>
		<!--Inicia tabla de cursos-->
		<!--<div class="row">
			<section class="container">
				<div class="table-responsive">

					<?php
						/*Consulta de los cursos que se encuentran en la base de datos*/			
						$consultarcursos = mysqli_query($conecto, "SELECT * FROM cursos");
						$cantidad=mysqli_num_rows($consultarcursos);
						$limites = 3;
						$tabla = "SELECT * FROM cursos";		
						$obtenerx = $_GET[agregarcurso];
						if($obtenerx != "si"){
					
						/*Muestra una Tabla de los cursos que se encuentran en la Base de Datos*/
						echo '<table class="post-body" width="100%;" border="1";>
								<thead>
									<tr>
										<td colspan="6" class="td1">
											<h2 class="h2"><img class="img-responsive img2" alt="Imagen responsive" src="images/cursos1.png">Cursos</h2>
										</td>
									</tr>
									<tr class="trestructura1 odd"> 
										<td class="tdestructura td1" align="center">#</td>
										<td class="tdestructura td1">Nombre del curso</td> 
										<td class="tdestructura1 td1">Descripción del curso</td>
										<td class="tdestructura td1">Estado del curso</td>
										<td class="tdestructura td1">Modificar</td>
										<td class="tdestructura td1">Eliminar</td>
									</tr>
								</thead>';
								/*Contador que permite saber la cantidad de cursos que se van a consultar*/
								$num = 0;

								/*Estructura para  la paginación*/
								$pagina = $_GET[pagina];
								if ($pagina == 0){
									$inicio = 0;
									$num=$inicio;
								}else{
									$ctdPaginas = ceil($cantidad/$limites); 
									$inicio = $pagina * $limites;
									$num=$inicio;
								}

								$consultarcursos2 = mysqli_query($conecto, "SELECT * FROM cursos LIMIT $inicio,$limites");	
								/*Estructura de repeticion WHILE que permite ejercutar una consulta de un array de la tabla cursos*/
								while($mostrarcursos=mysqli_fetch_array($consultarcursos2)){

								/*Ejecucion del Contador*/
								$num=$num+1;
					?>
								<tbody>
									<tr> 	
										<td class='tdestructura'><?=$num?></td>
			                            <td class='tdestructura'><?=$mostrarcursos[NombreCursos]?></td>
			                            <td class='tdestructura1'><?=$mostrarcursos[DescripcionCursos]?></td>
			                            <td class='tdestructura'>
											<?php if($mostrarcursos[CodigoEstadoCursos]=="1"){?>
				                            	<span class="estadoactivado">Activado</span><br>
												<input type='button' name="cambiarestados" value='Desactivar' onClick="window.location='?url=administrador&control=cursos&cambiarestado=<?=$mostrarcursos[CodigoEstadoCursos]?>&estado=<?=$mostrarcursos[EstadoEstadoCursos]?>'"/ >
											<?php }else{?>
		                            			<span class="estadodesactivado">Desactivado</span><br>
												<input type='button' name="cambiarestados" value='Activar' onClick="window.location ='?url=administrador&control=cursos&cambiarestado=<?=$mostrarcursos[CodigoEstadoCursos]?>&estado=<?=$mostrarcursos[EstadoEstadoCursos]?>'"/>
											<?php }?>	
										</td>					
										<td align="center">
											<a onClick="return confirm('Desea realmente modificar este curso?');" href="?url=administrador&control=cursos&modificarid=<?=$mostrarcursos[CodigoCursos]?>" ><img class="img-responsive img1" alt="Imagen responsive" src="images/editar.png"></td>
										<td align="center">
											<a  onClick="return confirm('Desea realmente eliminar este curso?');" href="?url=administrador&control=cursos&eliminarid=<?=$mostrarcursos[CodigoCursos]?>"><img class="img-responsive img1" alt="Imagen responsive" src="images/eliminar.png" ></td>
									</tr>
								<?php
								}
									echo "
										<tr class='trestructura'> 
											<td class='tdestructura' colspan='6'>",paginacion($tabla,$cantidad,$limites),"</td>
										</tr>";				
								echo "</tbody></table></div></section></div>";
						}

					?>-->


<h1>Cursos</h1>
<div class="datagrid"><table>
<thead><tr><th>#</th><th>header</th><th>header</th><th>header</th></tr></thead>
<tfoot><tr><td colspan="4"><div id="paging"><ul><li><a href="#"><span>Previous</span></a></li><li><a href="#" class="active"><span>1</span></a></li><li><a href="#"><span>2</span></a></li><li><a href="#"><span>3</span></a></li><li><a href="#"><span>4</span></a></li><li><a href="#"><span>5</span></a></li><li><a href="#"><span>Next</span></a></li></ul></div></tr></tfoot>
<tbody><tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr class="alt"><td>data</td><td>data</td><td>data</td><td>data</td></tr>
<tr><td>data</td><td>data</td><td>data</td><td>data</td></tr>
</tbody>
</table></div>
				
		<!--<div class="container">
			<br>
			<a href="#ventana1" class="btn btn-primary btn-lg" data-toggle="modal">Insertar curso</a>

			<div class="modal fade" id="ventana1">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Header de la ventana -->
						<!--<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-tittle text-center">
								Datos del curso
							</h4>
						</div>

						<!-- Contenido de la ventana-->
						<!--<div class="modal-body">
							<div class="table-responsive">
									<!--Muestra una Tabla de insertar el curso-->
									<!--<table align="center" class="table">
										<thead>
							                <tr>
								 				<td colspan="1" class="td"><strong>Nombre del curso:</strong></td>
												<td colspan="1" class="td"><input pattern="[a-zA-ZÑÍÓÚÉÁ0-9\sñáóíé]{3,30}" name="nombre" required title="El campo debe llenarse con letrasMay&uacute;sculas, min&uacute;sculas o n&uacute;meros, no se aceptan caracteres especiales, ejemplo: Soporte t&eacute;cnico 1" maxlength="30"></td>
											</tr>
									 	</thead>
										<tbody>
						           			<tr>
							                    <td colspan="1" class="td"><strong>Descripci&oacute;n del curso:</strong></td>
							                    <td class="td"><textarea pattern="[a-zñáóíéA-Z0-9\s]{3,300}" name="descripcion" required title="El campo puede llenarse con letras May&uacute;sculas, letras min&uacute;sculas o n&uacute;meros, ejemplo: Configuraci&oacute;n b&aacute;sica de cualquier sistema basado en GNU/Linux"></textarea></td>
						            		</tr>
						            	</tbody>
					            </table>
							</div>				
						</div>
						<!--Footer de la ventana-->
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-primary">Guardar</button>

							<button type="button" class="btn btn-defauld" data-dismiss="modal">Cerrar</button>
							</div>
					</div>
				</div>
			</div>
		</div>

					<nav>
						<div class="center-block">
							<ul class="pagination centrado">
								<li class="disabled"><a href="#">&laquo;<span class="sr-only">Anterior</span></a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&raquo;<span class="sr-only">Siguiente</span></a></li>
							</ul>
						</div>
					</nav>	-->
		<script src="../js/jquery.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</body>
</html>