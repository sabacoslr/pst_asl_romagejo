<?php
	require_once('configuracion/conectarse.conf.php');
?>
<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="UTF-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
		<title>Gráfico animado elegante con HTML5 y jQuery | elated.com</title>
		<link rel="stylesheet" href="../css/styles_reporte.css">
		<link rel="stylesheet" href="../css/bootstrap.min.css">
	</head>
	<body data-spy="scroll" data-target="#navegacion">
		<a href="#content" class="sr-only sr-only-focusable">Ir a la página principal</a>
		<header>
			<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
				<div id="barner">
					<div><img class="img-responsive" alt="" src="../img/cintillo.jpg"></div>
				</div>
<!-- Menú de Navegación -->
				<div class="container-fluid">
					<div class="navbar-header">
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navegacion">
							<span class="sr-only">Mostrar navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="navegacion">
						<ul class="nav navbar-nav navbar-left" role="tablist">
							<li><a class="navbar-brand" href="http://asl.fundacite-merida.gob.ve/"><b>ASL</b></a></li>
						</ul>
						<ul class="nav navbar-nav navbar-right" role="tablist">
							<li><a href="#">Inicio</a></li>
							<li><a href="#">Ayuda en línea</a></li>
						</ul>
					</div>
				</div>
			</nav>
		</header>
<!-- El contenido de la página principal -->
    	<div class="container" id="content" tabindex="-1">
    		<div class="main row">
				<div class="col-md-12">
<!-- Contenido: Gráfico Circular y la Tabla -->
					<div class="seccion" id="seccion1" style="height:100%;">
						<div class="text-center">
			              <div class="contenedor">
			                <article class="jumbotron"><h2>Cantidad de Postulantes por Curso</h2></article>
			                <div style="width: 50%">
			                	<canvas id="canvas" height="450" width="600"></canvas>
			                </div>
			                <script>
			                	var randomScalingFactor = function(){return Math.round(Math.random()*100)};
			                	var barChartData = {
			                		labels: [
			                			<?php
			                				$sql = "SELECT * FROM cursos";
			                				$consulta = mysqli_query($conecto, $sql);
			                				while($registros = mysqli_fetch_array($consulta)){
			                			?>
			                				'<?php echo utf8_encode($registros["NombreCursos"]); ?>',
			                			<?php
			                				}
			                			?>
			                		],
			                		datasets: [
										{
											fillColor: "rgba(151,187,205,0.5)",
											strokeColor: "rgba(151,187,205,0.8)",
											highlightFill: "rgba(151,187,205,0.75)",
											highlightStroke: "rgba(151,187,205,1)",
											data:
											<?php
			                	$sql = "SELECT Id_Cursos, COUNT(Id_Cursos) AS Id_Personas FROM historial GROUP BY Id_Cursos";
			                	$consulta = mysqli_query($conecto, $sql);
			                ?>
											[
											<?php
												while($registros = mysqli_fetch_array($consulta)){
											?>
											<?php
													echo utf8_encode($registros["Id_Personas"])
											?>,
											<?php
												}
											?>
											]
										}
									]
								}

								window.onload = function(){
									var ctx = document.getElementById("canvas").getContext("2d");
									window.myBar = new Chart(ctx).Bar(barChartData, {responsive: true});
								}
							</script>
			              </div>
						</div>
					</div>
				</div>
			</div>
    	</div>
<!-- Pie de Página -->
    	<footer></footer>

		<script src="../js/jquery-latest.js"></script>
		<script src="../js/main.js"></script>
		<script src="../js/Chart.js"></script>
		<script src="../js/bootstrap.min.js"></script>
	</body>
</html>
