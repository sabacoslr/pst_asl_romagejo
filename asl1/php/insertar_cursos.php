<!DOCTYPE html>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0 ">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/estilos.css">
	<title>Insertar curso</title>
</head>
<body class="container">
	<img class="img-responsive" alt="Imagen responsive" src="../images/header1.png">	
	<header>
		<nav class="navbar navbar-default" role="navigation">	
		<div class="container"></div>
			<div class="navbar-header"></div>
				<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion">
					<span class="sr-only">Desplegar / ocultar menu</span>
					<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
				</button>
    <!-- Inicia Menu-->
    		<div class="collapse navbar-collapse" id="navegacion">
			    	<ul class="nav navbar-nav">
			        	<li class="active"><a href="#">Inicio</a></li>

			        	<li>
			         		<a href="" class="dropdown-toggle" data-toggle="dropdown">Cursos <span class="caret"></span></a>
							<ul class="dropdown-menu aria-labelledby="dLabel"" role="menu" >
								<li><a href="#">Consultar cursos</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="../registro/administrador.php?url=administrador&control=cursos&agregarcurso=si">Insertar curso</a></li>
								<li role="separator" class="divider"></li>
							</ul>
			         	</li>

			         	<li class="dropdown">
			         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Postulantes <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Visualizar postulantes</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Insertar postulante</a></li>
								<li role="separator" class="divider"></li>
							</ul>
			         	</li>

			         	<li class="dropdown">
			         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Reportes estadísticos <span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Reporte de participante por curso</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Reporte por género</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Reporte por tipo de participante</a></li>
								<li role="separator" class="divider"></li>
							</ul>
			         	</li>

				        <li><a href="#">Ayuda</a></li>

				        <li class="dropdown">
				        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Configuración
				        	<span class="caret"></span></a>
							<ul class="dropdown-menu" role="menu">
								<li><a href="#">Perfil</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Niveles de usuarios</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Características generales de otras instituciones</a></li>
								<li role="separator" class="divider"></li>
								<li><a href="#">Salir</a></li>
								<li role="separator" class="divider"></li>
							</ul>
				        </li>
				   </ul>
  			</div><!--Fin del menú -->
  			</div>
		</nav>
	</header>

	<div class="container">
		<br>
		<a href="#ventana1" class="btn btn-primary btn-lg" data-toggle="modal">Insertar curso</a>

		<div class="modal fade" id="ventana1">
			<div class="modal-dialog">
				<div class="modal-content">
					<!-- Header de la ventana -->
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
						<h4 class="modal-tittle text-center">
							Datos del curso
						</h4>
					</div>

					<!-- Contenido de la ventana-->
					<div class="modal-body">
						<div class="table-responsive ">
								<!--Muestra una Tabla de insertar el curso-->
								<table align="center" class="imagen">
									<thead>
										<tr>
											<td colspan="3">
												<h2 class="h2"><img class="img-responsive img2" alt="Imagen responsive" src="../images/cursos1.png">Insertar curso</h2>
											</td>
										</tr>
						                <tr>
							 					<td colspan="1"><strong>Nombre del curso:</strong></td>
												<td colspan="1"><input pattern="[a-zA-ZÑÍÓÚÉÁ0-9\sñáóíé]{3,30}" name="nombre" class="input3" required title="El campo debe llenarse con letrasMay&uacute;sculas, min&uacute;sculas o n&uacute;meros, no se aceptan caracteres especiales, ejemplo: Soporte t&eacute;cnico 1" maxlength="30"></td>
						                        <td><a><span><br>El campo debe llenarse con la primera letra en May&uacute;scula seguida de letras min&uacute;sculas o n&uacute;meros, no se aceptan caracteres especiales, ejemplo: Soporte t&eacute;cnico 1. </span></a>
						                       	</td>
										</tr>
								 	</thead>
									<tbody>
					           			<tr>
						                    <td colspan="1"><strong>Descripci&oacute;n del curso:</strong></td>
						                    <td><textarea pattern="[a-zñáóíéA-Z0-9\s]{3,300}" name="descripcion" class="input3" required title="El campo puede llenarse con letras May&uacute;sculas, letras min&uacute;sculas o n&uacute;meros, ejemplo: Configuraci&oacute;n b&aacute;sica de cualquier sistema basado en GNU/Linux"></textarea></td>
						                     <td><a><span><br>El campo puede llenarse con letras May&uacute;sculas, letras min&uacute;sculas o n&uacute;meros, ejemplo: Configuraci&oacute;n b&aacute;sica de cualquier sistema basado en GNU/Linux.</span></a>
						                    </td>
					            		</tr>
					            	</tbody>
				            </table>
						</div>				
					</div>

					<!--Footer de la ventana-->
					<div class="modal-footer">
						<button type="button" class="btn btn-primary">Guardar</button>

						<button type="button" class="btn btn-defauld" data-dismiss="modal">Cerrar</button>
					</div>
				</div>
			</div>
		</div>
	</div>

		<section class="container">
			
			
			</div>
			</section>
		</div>
	</section>






	<footer></footer>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</body>
</html>