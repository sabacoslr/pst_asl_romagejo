<!DOCTYPE html>
<html lang="es">
		<head>
			<meta charset="UTF-8">
			<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0 ">
			<link rel="stylesheet" href="../css/bootstrap.min.css">
			<link rel="stylesheet" href="../css/estilos.css">
			<link rel="stylesheet" href="../css/estilos2.css">
			<title>Consultar cursos</title>
		</head>
	<body class="container">
	<?php
			/*Conexión con la base de datos*/
			require_once("../configuracion/conectarse.conf.php");
			/*Diferentes funciones que tiene el sistema*/
			require_once("../configuracion/funciones.conf.php");
			/*Para que la sesión este activada*/
			@session_start();
		?>
		<!-- Banner del sistema-->
			<img class="img-responsive" alt="Imagen responsive" src="../img/header1.png">
				<!--Fin del Banner-->


				<!--Ejemplo tablas curso -->
				<?php if($_GET[control]==""){

		}

			/*ADMINISTRADORES*/
			$obtener2=$_GET['control'];
		?>
		<!--Inicia tabla de cursos-->
		<hr>

		<h1 class="h1" align="center">Cursos</h1>
		<div class="row">
			<section class="container">
				<div>

					<?php
						
						/*Consulta de los cursos que se encuentran en la base de datos*/			
						$consultarcursos = mysqli_query($conecto, "SELECT * FROM cursos");
						$cantidad=mysqli_num_rows($consultarcursos);
						$limites = 4;
						$tabla = "SELECT * FROM cursos";		
						$obtenerx = $_GET[agregarcurso];
						if($obtenerx != "si"){
					
						/*Muestra una Tabla de los cursos que se encuentran en la Base de Datos*/
						echo '<div class="datagrid"> <table class="letras table-responsive"  width="100%;" border="1">
								<thead>
									<tr class="trestructura1"> 
										<th class="tdestructura" align="center">#</th>
										<th class="tdestructura">Nombre</th>
										<th class="tdestructura1">Descripción</th>
									</tr>
								</thead>';
								/*Contador que permite saber la cantidad de cursos que se van a consultar*/
								$num = 0;

								/*Estructura para  la paginación*/
								$pagina = $_GET[pagina];
								if ($pagina == 0){
									$inicio = 0;
									$num=$inicio;
								}else{
									$ctdPaginas = ceil($cantidad/$limites); 
									$inicio = $pagina * $limites;
									$num=$inicio;
								}

								$consultarcursos2 = mysqli_query($conecto, "SELECT * FROM cursos LIMIT $inicio,$limites");	
								/*Estructura de repeticion WHILE que permite ejercutar una consulta de un array de la tabla cursos*/
								while($mostrarcursos=mysqli_fetch_array($consultarcursos2)){

								/*Ejecucion del Contador*/
								$num=$num+1;
					?>
								<tbody>
									<tr> 	
										<td class="tdestructura"><?=$num?></td>
			                            <td class='tdestructura'><?=$mostrarcursos[NombreCursos]?></td>
			                            <td class="tdestructura2"><?=$mostrarcursos[DescripcionCursos]?></td>
										</td>
									</tr>
								<?php
								}
								
								echo "	
									<tfoot align='center'>
										<tr>
											<td colspan='6'>
												<div id='paging'>
													<ul>
														<li><a href='#''><span>Previous</span></a></li>
														<li><a href='#> class='active'><span>1</span></a></li>
														<li><a href='#'><span>2</span></a></li>
														<li><a href='#'><span>3</span></a></li>
														<li><a href='#'><span>4</span></a></li>
														<li><a href='#'><span>5</span></a></li>
														<li><a href='#'><span>Next</span></a></li>
													</ul>
												</div>
											</td>
										</tr>
									</tfoot>";
								echo "</tbody></table></div></section></div>";
						}
					


					?>
							<!-- Inicio de la propuesta de cursos -->
							<hr>
				<div class="container">
				<h4 class="h4" align="center">
					Si deseas que existan nuevos cursos para que sean postulados pulsa el boton y haz tu propuesta a la página!! 
				</h4>
	
				</div>
		<div>
		<br>
		<a href="#ventana" class="btn btn-primary btn-lg " data-toggle="modal">Proponer cursos</a>

		<div class="modal fade" id="ventana">
			<div class="modal-dialog">
			<div class="modal-content">
			<!-- Cabeza de la ventana-->
			<div class="modal-header">
			<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4>Escribe el curso o cursos que deseas que se postulen</h4>

				
				</div>	
				<!-- Contenido de la ventana-->
				<div  class="modal-body">
			<form name="form2" id="form2" method="post" action="insertar_propuesta.php" class="form-horizontal">
				<label for="cursoP"></label>
				<textarea class="form-control" name="cursoP" id="cursoP"></textarea>
					<button type="submit" class="btn btn-primary" onclick=location='insertar_propuesta.php' title="Permite guardar los datos del formulario">Guardar cambios</button>
            
            </form>
				</div>
				<!--Pie de la ventana -->
				<div class="modal-footer">
			
				</div>
			</div>	
		</div>
	</div>	
</div>
<br>
<br>
<br>
<!-- Fin de la propuesta de cursos-->
	 <script src="../js/jquery.js"></script>
   <script src="../js/bootstrap.min.js"></script>
 </body>
</html>