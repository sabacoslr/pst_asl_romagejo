
<?php

session_start();
//inicio de sesion
if (isset($_SESSION['autenticado']))
//verificaciond el estado de la sesion
{
   if ( $_SESSION['autenticado'] == TRUE )
   
   // Cuando la sesion esta iniciada ingresa al contenido de la pagina
   {     
           
  


/* DEBEN INCLUIR ESTE ARCHIVO ES TODAS LAS PAGIANS PHP DE SU SISTEMA, EXCEPTO EL index.php */
?>




<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Sistema de postulación de cursos</title>
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../estilo.css">
	<link rel="stylesheet" href="../css/bootstrap.min.css">

	<style>
      	.nav1 {
				background: #2464CB;
				height: 80px;
			}

	</style>
</head>
<body >
<div class="container">
<!--Cintillo de la ASL-->
<img class="img-responsive" alt="Imagen responsive" src="../img/header1.png">
	
<!--Inicio de la Barra de menu de postulacion de cursos-->

		<div class="container-fuid">
		<br>
			 <header>
			  <div class="col-md-12">
				 <nav class="navbar navbar-default navbar" role="navigation">
				   <div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegar">
						<span class="sr-only">Menú</span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>
						<span class="icon-bar"></span>	
						</button>
					</div>
					 <div class="collapse navbar-collapse" id="navegar">
					   <ul class="nav navbar-nav">
						<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown">Conócenos<span class="caret"></span>
						</a>
						<ul class="dropdown-menu">
						<li><a href="#">Misión</a></li>
						<li class="divider"></li>
						<li><a href="#">Visión</a></li>
						<li class="divider"></li>
						<li><a href="#">Historia</a></li>
					     </ul>
						</li>
						<li><a href="Consultar_cursos.php">Cursos ofertados</a></li>
						<li><a href="">Postulación de cursos</a></li>
						<li><a href="">Ayuda en linea</a></li>

                        <?php if($_SESSION['nivel']==0 or $_SESSION['nivel']==1) {
							
							 ?><li><a href="index.php"> Panel de administrador </a></li> <?php } ?>
						<li><a href="">Cerrar sesion</a></li>
					   </ul>
					</div>
				</div>
			</nav>
		</header>
		</div>
		<!-- GALERIA DEL SLIDE -->
<div><br>
	<div class="col-md-12">
		<div id="carousel-1" class="carousel slide" data-ride="carousel">
<!-- Indicadores -->
			<ol class="carousel-indicators">
				<li data-target="#carousel-1" data-slide-to="0" class="active"></li>
				<li data-target="#carousel-1" data-slide-to="1"></li>
				<li data-target="#carousel-1" data-slide-to="2"></li>
				<li data-target="#carousel-1" data-slide-to="3"></li>
				<li data-target="#carousel-1" data-slide-to="4"></li>
				<li data-target="#carousel-1" data-slide-to="5"></li>
				<li data-target="#carousel-1" data-slide-to="6"></li>
			</ol>
<!-- Contenedor de los slide -->
			<div class="carousel-inner" role="listbox">
				<div class="item active">
					<img src="../img/1.jpg" class="img-responsive" alt="">
					<div class="carousel-caption hidden-xs hidden-sm">
						<p>ACADEMIA DEL SOFTWARE LIBRE</p>
					</div>
				</div>
				<div class="item">
					<img src="../img/2.jpg" class="img-responsive" alt="">
					<div class="carousel-caption hidden-xs hidden-sm">
						<p>ACADEMIA DEL SOFTWARE LIBRE</p>
					</div>
				</div>
				<div class="item">
					<img src="../img/3.jpg" class="img-responsive" alt="">
					<div class="carousel-caption hidden-xs hidden-sm">
						<p>ACADEMIA DEL SOFTWARE LIBRE</p>
					</div>
				</div>
				<div class="item">
					<img src="../img/4.jpg" class="img-responsive" alt="">
					<div class="carousel-caption hidden-xs hidden-sm">
						<p>ACADEMIA DEL SOFTWARE LIBRE</p>
					</div>
				</div>
				<div class="item">
					<img src="../img/5.jpg" class="img-responsive" alt="">
					<div class="carousel-caption hidden-xs hidden-sm">
						<p>ACADEMIA DEL SOFTWARE LIBRE</p>
					</div>
				</div>
				<div class="item">
					<img src="../img/6.jpg" class="img-responsive" alt="">
					<div class="carousel-caption hidden-xs hidden-sm">
						<p>ACADEMIA DEL SOFTWARE LIBRE</p>
					</div>
				</div>
				<div class="item">
					<img src="../img/7.jpg" class="img-responsive" alt="">
					<div class="carousel-caption hidden-xs hidden-sm">
						<p>ACADEMIA DEL SOFTWARE LIBRE</p>
					</div>
				</div>
<!-- Controles -->
			<a href="#carousel-1" class="left carousel-control" role="button" data-slide="prev">
				<span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
				<span class="sr-only">Anterior</span>
			</a>
			<a href="#carousel-1" class="right carousel-control" role="button" data-slide="next">
				<span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
				<span class="sr-only">Siguiente</span>
			</a>
		</div>
	</div>
	<br><br><br><br><br><br><br><br><br>
</div>
<!-- Fin del Slide -->
   

	<footer>
	
				<nav class="navbar navbar-default navbar-fixed-bottom nav1" role="navigation">
					<div class="navbar-footer">
						<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navegacion">
							<span class="sr-only">Mostrar navegación</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
				</nav>

		</footer>
		</div>
	<script src="../js/jquery.js"></script>
	<script src="../js/bootstrap.min.js"></script>
</body>
</html>


<?php
}
 }
else

// Cuando la sesion no esta iniciada muestra el error
{
      echo "<br><br><br><div ALIGN=center STYLE='font-size:30px; font-weight:bold'>
                           NO TIENE ACCESO...
                        </div>";
      exit();    
}

?>