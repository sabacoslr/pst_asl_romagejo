<!DOCTYPE html>
    <?php  
     require_once("../configuracion/conectarse.conf.php");
    ?>
<html lang="es">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
	<link rel="stylesheet" href="../css/bootstrap.min.css">
	<link rel="stylesheet" href="../css/estilos.css">
	<link rel="stylesheet" href="../validacion/css/bootstrapValidator.css"/>
	<script type="text/javascript" src="../js/jquery.min.js"></script>
	<script type="text/javascript" src="../js/jquery.js"></script>
    <script type="text/javascript" src="../js/bootstrap.min.js"></script>
    <script type="text/javascript" src="../validacion/js/bootstrapValidator.js"></script> 
   	<title>Registrarse</title>
</head>
<body>

	<div class="container">
		<img class="header1" src="../images/header1.png">	
		<br><br>
		<div><div class='bienvenidos'><?php echo 'Bienvenido usuario: <b>'.$_SESSION['NombreUsuario'].'</b>'?></span></div>
			
		</div>
		<div class="letrasdiv1">
			Debes completar todos los campos obligatorios (marcados con un * ) en el formulario
		</div>
		<div class="row">
			<!--Ventana emergente de la ayuda en linea-->
			<div class="input-group input-group-md ayuda">
				<a href="#modal1" title="Ayuda en linea" data-toggle="modal"><span class="input-group-addon" id="sizing-addon1"><img src="../images/ayuda.png" 
				class="img"/>Ayuda en Linea</span></a>
			</div>
			<div class="modal fade" id="modal1"><br><br><br>
				<div class="modal-dialog">
					<div class="modal-content">
						<!--Header de la ventana-->
						<div class="modal-header">
							<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-title">Ayuda en linea para postularse en los cursos de la Academia de Software Libre</h4>
						</div>
						<!--Contenido de la ventana-->
						<div class="modal-body">
							<center><a href="javascript:void(0);" title="Clic para ver el video" onclick="window.open('https://www.youtube.com/watch?v=wH2LBi4OHDM','sickg', 'left=390, top=200, width=550, height=354, toolbar=0, resizable=1')">
							<div align="center"><img src="images/videos.ico" title="Clic para ver el vídeo" /></div></a></center>
						</div>
						<!--Footer de la ventana-->
						<div class="modal-footer">
							<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
						</div>
					</div>
				</div>
			</div><br><br>
			<!--Inicio del formulario-->
				<form name="form1" id="form1" method="post" action="insertar_datos.php" class="form-horizontal">
			        <div>
			            <fieldset class="mover col-xs-11 col-sm-5 col-md-5 col-lg-5">
			            	<legend>DATOS PERSONALES</legend>
			            	<table class="tabla tt-wrapper" align="center"><br>
			            		<div class="form-group">
                                    <label class="control-label col-lg-4">C&eacute;dula: (*)</label>
                                        <div class="col-lg-7">
                                            <input type="text" name="cedu" autocomplete="off" class="form-control " maxlength="8" placeholder="C&eacute;dula" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo debe contener tu cédula de identidad, sólo admite n&uacute;meros, ejemplo: 22080100">
                                        </div>                              
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Apellido: (*)</label>
                                        <div class="col-lg-7">
                                            <input type="text" name="ape" class="form-control" maxlength="25" placeholder="Apellido" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo sólo admite letras, la primera letra debe ser May&uacute;scula y las dem&aacute;s en min&uacute;sculas, ejemplo: Urdaneta.">
                                        </div>                              
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Nombre: (*)</label>
                                        <div class="col-lg-7">
                                            <input type="text" name="nom" class="form-control" maxlength="25" placeholder="Nombre" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo sólo admite letras, a primera letra debe ser May&uacute;scula y las dem&aacute;s en min&uacute;sculas, ejemplo: Rosa" >
                                        </div>                              
                                </div>                           
                                <div class="form-group">
                                        <label class="control-label col-lg-4">Género: (*)</label>
                                        <div class="col-lg-7" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Seleccione el género al que perteneces">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  name="sex" required  value="Masculino">Masculino
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  name="sex" required value="Femenino">Femenino
                                                </label>
                                            </div>
                                        </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-3">Telefono: (*)</label>
                                        <div class="col-lg-3">
                                            <select name="tipos" id="select2" class="form-control" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Seleccione un código">
						              	    	<option value="0">-- --</option> 
													<?php 
																$consultartipos = @mysqli_query($conecto, "SELECT * FROM tipostelefonos;");
																while ($imprimir = mysqli_fetch_array($consultartipos)){
													?> 
										            			<option value="<?=$imprimir[CodigoTelefono];?>"><?=$imprimir[TiposTelefonos];?></option>
													<?php
																}
													?>			
											</select>
										</div>
										<div class="col-lg-5">
											<input type="text" name="tele" class="form-control" maxlength="13" placeholder="Telefono" pattern="[0-9]{4}[-]{1}[0-9]{7}" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Por favor llene este campo con 4 números, seguido de un guion (-) y luego los 7 números restantes, Ejemplo: 0000-0000000">
										</div>                               
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Grado de instrucci&oacute;n: (*)</label>
                                        <div class="col-lg-7">
                                            <select name="profe" id="select4" class="form-control" rel="popover" data-container="body" data-toggle="popover"
											data-placement="top" title="Seleccione una de las opciones, esta parte depende del grado de instrucci&oacute;n que tengas">
					              	    		<option value="0">Seleccione</option> 
													<?php 
																$consultarprofesion = @mysqli_query($conecto, "SELECT * FROM grado_instruccion;");
																while ($imprimir = mysqli_fetch_array($consultarprofesion)){
													?> 
										            			<option value="<?=$imprimir[CodigoGrado];?>"><?=$imprimir[NombreGrado];?></option>
													<?php
																}
													?>			
											</select>
                                        </div>                              
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4" >Tipo de participante: (*)</label>
                                        <div class="col-lg-7" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Seleccione una opci&oacute;n">
                                            <?php
											$participante = mysqli_query ($conecto, "SELECT * FROM participante");
											while ($imprimirparticipante = mysqli_fetch_array($participante))		{	
											if ($imprimirparticipante[Participante]=="Estudiante" or $imprimirparticipante[CodigoParticipante]=="1"){
										
												?> <input type="radio" name="par" id="radio1" value="<?=$imprimirparticipante[CodigoParticipante]?>"onClick="document.getElementById('estudiante').hidden=false"><?=$imprimirparticipante[Participante]?><br> <?php
												
											}else {
											?> <input type="radio" name="par" id="radio1" value="<?=$imprimirparticipante[CodigoParticipante]?>"onClick="document.getElementById('estudiante').hidden=true"><?=$imprimirparticipante[Participante]?><br> <?php
											}}
										?>
                                        </div>                              
                                </div>
                                <div class="form-group"  id="estudiante" hidden="true">
                                    <label class="control-label col-lg-4">Instituci&oacute;n:(*)</label>
                                        <div class="col-lg-7">
                                            <input type="text" name="institu" class="form-control" maxlength="100" placeholder="Institucion" 
				                			rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo sólo admite letras, debe colocar la instituci&oacute;n que perteneces" >
                                        </div>                              
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Correo electrónico: (*)</label>
                                        <div class="col-lg-7">
                                            <input type="text" name="cor" class="form-control" maxlength="50" placeholder="Correo electronico" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Por favor llene este campo con letras, seguidas de un @, luego continua con letras, un punto (.) y letras, Ejemplo:urdaneta@gmail.com" >
                                        </div>                              
                                </div> 
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Provincia: (*)</label>
                                        <div class="col-lg-7">
                                            <select name="esta" id="select1" class="form-control" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Seleccione una de las opciones, dependiendo a la provincia que perteneces">
					              	    	<option value="0">Seleccione</option> 
												<?php 
															$consultarestados =   @mysqli_query($conecto, "SELECT * FROM provincia;");
															while ($imprimir=mysqli_fetch_array($consultarestados)){
												?> 
									            			<option value="<?=$imprimir[CodigoProvincia];?>"><?=$imprimir[NombreEstados];?></option><?php
															}
												?>			
											</select>
                                        </div>                              
                                </div>            
			            		<tr>
			            			<td>
			            				<br>
			            			</td>
			            		</tr>
			            	</table>
			            </fieldset>
			        </div>
			        <div>
			           <fieldset class="col-xs-11 col-sm-5 col-md-5 col-lg-5">
			            	<legend>DATOS LOGIN</legend>
			            	<table class="tabla tt-wrapper" align="center"><br>
			            		<div class="form-group">
                                    <label class="control-label col-lg-4">Usuario: (*)</label>
                                        <div class="col-lg-7">
                                            <input type="text" name="usuario" id="usuario" autocomplete="off" class="form-control" maxlength="25" placeholder="Usuario"
											rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo sólo admite letras, m&iacute;nimo 4 y m&aacute;ximo 10, ejemplo: Usuario">
                                        </div>                              
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Contrase&ntilde;a: (*)</label>
                                        <div class="col-lg-7">
                                            <input type="password" name="clave" id="clave" class="form-control" maxlength="20" placeholder="Contraseña" 
											rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo admite letras, n&uacute;meros y caracteres especiales(*)&nbsp;(,)&nbsp;(-)&nbsp;(/)&nbsp;<br>(@)&nbsp;(.)&nbsp;, m&iacute;nimo 8 y m&aacute;ximo 16 caracteres ejemplo: contrasena1*">
                                        </div>                              
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Repite contrase&ntilde;a: (*)</label>
                                        <div class="col-lg-7">
                                            <input type="password" name="vclave" class="form-control" maxlength="20" placeholder="Repite contraseña" 
											 rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo debe llevar la misma contrase&ntilde;a que coloco en el campo de arriba ejemplo: contrasena1*" >
                                        </div>                              
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Pregunta secreta: (*)</label>
                                        <div class="col-lg-7" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Por favor seleccione una pregunta secreta">
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  name="secre" value="nombre de la madre">Nombre de la madre
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  name="secre" value="mascota favorita">Mascota favorita
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  name="secre" value="color favorito">Color favorito
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  name="secre" value="lugar de nacimiento">Lugar de nacimiento
                                                </label>
                                            </div>
                                            <div class="radio">
                                                <label>
                                                    <input type="radio"  name="secre" value="musica favorita">M&uacute;sica favorita
                                                </label>
                                            </div>
                                        </div>                             
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-lg-4">Respuesta secreta: (*)</label>
                                        <div class="col-lg-7">
	                                       <input type="text" name="respues" class="form-control" maxlength="25" placeholder="Respuesta secreta" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="El campo debe llevar la respuesta secreta sólo admite letras y numeros" >
                                        </div>                              
                                </div>
								<tr><td><br></td></tr>
			            		<tr>
									<td colspan="3" align="center">
										<button type="submit" onclick=location='insertar_datos.php' class="botones2" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Permite guardar los datos del formulario">
										<span class="glyphicon glyphicon-floppy-saved"> Guardar</span></button>
										<button type="reset" class="botones2" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Permite borrar todos los campos del formulario">
										<span class="glyphicon glyphicon-trash"> Restablecer</span></button><br>
		        						</br>
		        						<button onclick=location='../index.php' class="botones2" rel="popover" data-container="body" data-toggle="popover" data-placement="top" title="Permite regresar a la p&aacute;gina principal">
		        						<span class="glyphicon glyphicon-arrow-left"> Atr&aacute;s</span></button>
									</td>
								</tr>
								<tr>
			            			<td>
			            				<br>
			            			</td>
			            		</tr>
			            	</table>
			            </fieldset>
			        </div>
			    </form>
		    </div>
		</div>
	<script type="text/javascript" src="../validacion/validar_formulario.js"></script>
	<script>
		$('[rel="popover"]').popover({
			trigger:'hover', //click | hover | focus | manual
			html: true,
			delay: 100,
		})
	</script>
</body>
</html>