<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0 ">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/estilos.css">
		<title>Postulantes</title>
	</head>
	<body class="container">
		<?php
			/*Conexión con la base de datos*/
			require_once("./configuracion/conectarse.conf.php");
			/*Diferentes funciones que tiene el sistema*/
			require_once("./configuracion/funciones.conf.php");
			/*Para que la sesión este activada*/
			@session_start();
		?>

		<!-- Banner del sistema-->
		<img class="img-responsive" alt="Imagen responsive" src="../images/header1.png">	
		<!--Fin del Banner-->

		<header>
			<nav class="navbar navbar-default" role="navigation">	
			<div class="container">
				<!--Botón que se coloca cuando las pantallas son mas pequeñas-->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion">
						<span class="sr-only">Desplegar / ocultar menu</span>
						<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
				</div>
				<!--Fin del bótón-->

	    		<!-- Inicia Menu-->
	    		<div class="collapse navbar-collapse" id="navegacion">
				    	<ul class="nav navbar-nav">
				        	<li class="active"><a href="#">Inicio</a></li>

				        	<li>
				         		<a href="" class="dropdown-toggle" data-toggle="dropdown">Cursos <span class="caret"></span></a>
								<ul class="dropdown-menu aria-labelledby="dLabel"" role="menu" >
									<li><a href="#">Consultar cursos</a></li>
								</ul>
				         	</li>

				         	<li class="dropdown">
				         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Postulantes <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Visualizar postulantes</a></li>
								</ul>
				         	</li>

				         	<li class="dropdown">
				         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Reportes estadísticos <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Reporte de participante por curso</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Reporte por género</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Reporte por tipo de participante</a></li>
									<li role="separator" class="divider"></li>
								</ul>
				         	</li>

					        <li class="dropdown">
								<!--Ventana emergente de la ayuda en linea-->
								<a href="#modal1" class="dropdown-toggle" data-toggle="modal">Ayuda en Línea</a>
								<div class="modal fade" id="modal1"><br><br><br>
									<div class="modal-dialog">
									<div class="modal-content">
									<!--Header de la ventana-->
										<div class="modal-header">
											<button tyle="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
											<h4 class="modal-title">Ayuda en linea para postularse en los cursos de la Academia de Software Libre</h4>
										</div>
										<!--Contenido de la ventana-->
										<div class="modal-body">
											<center><a href="javascript:void(0);" title="Clic para ver el video" onclick="window.open('https://www.youtube.com/watch?v=wH2LBi4OHDM','sickg', 'left=390, top=200, width=550, height=354, toolbar=0, resizable=1')">
											<div align="center"><img src="../images/videos.ico" class="img-responsive" title="Clic para ver el vídeo" /></div></a></center>
										</div>
										<!--Footer de la ventana-->
											<div class="modal-footer">
											<button type="button" class="btn btn-primary" data-dismiss="modal">Cerrar</button>
										</div>
								</div></div></div>
							</li>

					        <li class="dropdown">
					        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Configuración
					        	<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Perfil</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Niveles de usuarios</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Características generales de otras instituciones</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Salir</a></li>
									<li role="separator" class="divider"></li>
								</ul>
					        </li>
					   </ul>
	  			</div>
	  			<!--Fin del menú -->
	  			</div>
				</nav>
		</header>
			
		<?php if($_GET[control]==""){

		}
			/*ADMINISTRADORES*/
			$obtener2=$_GET['control'];
		?>
		<!--Inicia tabla de cursos-->
		<hr>
		<h1 class="h1" align="center">POSTULANTES</h1><br>
		<div class="row">
			<section class="container">
				<form name="filtro1" method="post" style="float:right">
						<p class="p">
							<select name="filtrar1">
								<option>Seleccione el curso</option>
								<?php
									$cfiltrar = mysqli_query($conecto,"SELECT * FROM cursos ORDER by NombreCursos ASC");
									while ($carg = mysqli_fetch_array($cfiltrar)){
								?>
								<option value="<?=$carg[Id_Cursos]?>"><?=$carg[NombreCursos]?></option>
							    <?php }	?>          
	    					</select>
							<input type="submit" name="filtrar" value="Consultar">
						</p>
				</form>

				<?php						   
				if(isset($_POST[filtrar])){
					$tipocurso = $_POST[filtrar1];
					$consultarusuario = mysqli_query($conecto, "SELECT * FROM personas, provincia, participante, grado_instruccion, realizan_postulacion, cursos, telefono
					WHERE 
						realizan_postulacion.Id_Cursos = '$tipocurso' AND 
						realizan_postulacion.Id_personas = personas.Id_Personas AND		
						personas.CodigoProvincia = provincia.CodigoProvincia AND 
						personas.CodigoParticipante = participante.CodigoParticipante AND 
						personas.CodigoGrado = grado_instruccion.CodigoGrado AND
						realizan_postulacion.Id_Personas = personas.Id_Personas AND
						realizan_postulacion.Id_Cursos = cursos.Id_Cursos AND
						telefono.Id_Personas = personas.Id_Personas") or die(mysqli_error($conecto));
					$cont =  mysqli_num_rows($consultarusuario);
					
				} else {
					$consultarusuario = mysqli_query($conecto, "SELECT * FROM personas, provincia, participante, grado_instruccion, realizan_postulacion, cursos, telefono  WHERE 
		  		    realizan_postulacion.Id_Personas = personas.Id_Personas AND		
					personas.CodigoProvincia = provincia.CodigoProvincia AND 
					personas.CodigoParticipante = participante.CodigoParticipante AND 
					personas.CodigoGrado = grado_instruccion.CodigoGrado  AND		
					realizan_postulacion.Id_Personas = personas.Id_Personas AND
					realizan_postulacion.Id_Cursos = cursos.Id_Cursos AND
					telefono.Id_Personas = personas.Id_Personas	ORDER BY realizan_postulacion.Id_Postulacion") or die(mysqli_error($conecto));
				}
					
						/*Muestra una Tabla de los cursos que se encuentran en la Base de Datos*/
						echo '<div class="datagrid" id="no-more-tables"> <table class="col-md-12 table-bordered table-responsive table-condensed cf">
								<thead class="cf">
									<tr class="trestructura1"> 
										<th class="tdestructura" align="center">#</th>
										<th class="tdestructura">Cédula</th>
										<th class="tdestructura">Apellido</th>
										<th class="tdestructura">Nombre</th>
										<th class="tdestructura">Género</th>
										<th class="tdestructura">Correo</th>
										<th class="tdestructura">Teléfono</th>
										<th class="tdestructura">Provincia</th>
										<th class="tdestructura">Tipo de Participante</th>
										<th class="tdestructura">Grado de Instrucción</th>
										<th class="tdestructura">Institución</th>
										<th class="tdestructura">Funciones</th>
										<th class="tdestructura">Postulación</th>
									</tr>
								</thead>';
								/*Contador que permite saber la cantidad de cursos que se van a consultar*/
								$num = 0;
								while($mostrarusuario=mysqli_fetch_array($consultarusuario)){
								/*Ejecucion del Contador*/
								$num=$num+1;
					?>
								<tbody>
									<tr> 	
										<td class="tdestructura" data-title="#"><?=$num?></td>
			                            <td class="tdestructura" data-title="Cédula:"><?=$mostrarusuario[CedulaPersonas]?></td>
			                            <td class="tdestructura" data-title="Apellido:"><?=$mostrarusuario[ApellidoPersonas]?></td>
			                            <td class="tdestructura" data-title="Nombre:"><?=$mostrarusuario[NombrePersonas]?></td>	
			                            <td class="tdestructura" data-title="Género:"><?=$mostrarusuario[SexoPersonas]?></td>
			                            <td class="tdestructura" data-title="Correo:"><?=$mostrarusuario[CorreoPersonas]?></td>		
			                            <td class="tdestructura" data-title="Teléfono:"><?=$mostrarusuario[TelefonoPersonas]?></td>			                            	                            
			                            <td class="tdestructura" data-title="Provincia:"><?=$mostrarusuario[NombreEstados]?></td>	
			                            <td class="tdestructura" data-title="Participante:"><?=$mostrarusuario[Participante]?></td>
			                      		<td class="tdestructura" data-title="Instrucción:"><?=$mostrarusuario[NombreGrado]?></td>
			                      		<td class="tdestructura" data-title="Intituciones:"><?=$mostrarusuario[NombreInstitucion]?></td>
			                            
										<td align="center" data-title="Funciones:"><br>
												<a data-toggle="modal" onClick="return confirm('¿Desea realmente modificar al postulante?');"  href="?url=administrador&control=cursos&modificarid=<?=$mostrarcursos[CodigoCursos]?> #ventana1" ><img class="img-responsive img1" alt="Imagen responsive" src="images/editar.png" title="Permite modificar los datos del postulante"></a>
													<div class="col-md-12">
													<div class="modal fade" id="ventana1">
														<div class="modal-dialog">
															<div class="modal-content">
																<!-- Header de la ventana -->
																<div class="modal-header">
																	<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
																	<h4 class="modal-tittle text-center">Modificación de datos</h4>
																</div>

																<!-- Contenido de la ventana-->
																<div class="modal-body">
																<div class="ventana" id="no-more-tables"> 
																<?php

																		$consul =  mysqli_query($conecto, "SELECT * FROM personas");
																		$ejecutarconsulta = mysqli_fetch_array($consul) ;
																											
																		$consultar6 = mysqli_query($conecto, "SELECT * FROM telefono ");
																		$ejecutarconsulta4 = mysqli_fetch_array ($consultar6);
																?>

																	<form name="formulario3" action="registra.php" method="post">
																	<table class="col-md-12 table-responsive table-condensed tamano1">
																		<thead>
																			<tr>
																				<th>Cédula</th>
																				<th><input type="text" value="<?=$ejecutarconsulta[CedulaPersonas];?>" disabled></th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Apellido</th>
																				<th><input type="text" value="<?=$ejecutarconsulta[ApellidoPersonas];?>"></th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Nombre</th>
																				<th><input type="text" value="<?=$ejecutarconsulta[NombrePersonas];?>"></th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Género</th>
																				<th>
																					<?php 
																						if ($ejecutarconsulta[SexoPersonas] == "F"){						
																	                        	echo '<input type="radio"  value="F" name="sex" checked required>Femenino<br>
																								<input type="radio" value="M" name="sex" required> Masculino<br></td>';
																						}else {
																								echo '<input type="radio"  value="F" name="sex" required>Femenino<br>
																								<input type="radio" value="M" name="sex" checked required>Masculino<br></td>';
																						}
																					?>
																				</th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Correo</th>
																				<th><input type="text" value="<?=$ejecutarconsulta[CorreoPersonas];?>"></th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Teléfono</th>
																				<th><input type="text" value="<?=$ejecutarconsulta4[TelefonoPersonas];?>"></th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Provincia</th>
																				<th>
																					<?php
																						$estad = $ejecutarconsulta[CodigoProvincia];
																						$bestado = mysqli_query ($conecto, "SELECT * FROM provincia WHERE CodigoProvincia = '$estad'");
																					 	$bestado2 = mysqli_query ($conecto, "SELECT * FROM provincia");
																					 	$restado = mysqli_fetch_array ($bestado);
																					?>
																		        		<select name="estados" class="input2">
																		            	<option value="<?=$estad?>"><?=$restado[NombreEstados]?> </option>
																		        	<?php
																						while ($restado2 = mysqli_fetch_array($bestado2)){
																							if ($restado[NombreEstados]!=$restado2[NombreEstados]){
																							echo '<option value='.$restado2[CodigoProvincia].' >'.$restado2[NombreEstados].'</option>';}
																						}               
																					?>
																						</select>
																				</th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Tipo de Participante</th>
																				<th>
																					<?php 
																						if ($ejecutarconsulta[CodigoParticipante] == 1){
																					?>
																		                    <input type="radio" name="tipo" id="radio1" required value="1"onClick="document.getElementById('estudiante').hidden=false" checked>Funcionario P&uacute;blica<br>
																							<input type="radio" name="tipo" id="radio2" required value="2" onClick="document.getElementById('estudiante').hidden=false">Estudiante <br>
																							<input type="radio" name="tipo" id="radio3" required value="3" onClick="document.getElementById('estudiante').hidden=true">P&uacute;blico en General<br>
																	                        
																	                <?php																							
																						}elseif($ejecutarconsulta[CodigoParticipante] == 2){
																					?>
																	                        <input type="radio" name="tipo" id="radio1" required value="1"onClick="document.getElementById('estudiante').hidden=false" >Instituci&oacute;n P&uacute;blica<br>
																							<input type="radio" name="tipo" id="radio2" required value="2" onClick="document.getElementById('estudiante').hidden=false" checked>Estudiante <br>
																							<input type="radio" name="tipo" id="radio3" required value="3" onClick="document.getElementById('estudiante').hidden=true">P&uacute;blico en General<br>
																	                <?php
																						}else{
																					?>
																	                        <input type="radio" name="tipo" id="radio1" required value="1"onClick="document.getElementById('estudiante').hidden=false"  >Instituci&oacute;n P&uacute;blica<br>
																							<input type="radio" name="tipo" id="radio2" required value="2" onClick="document.getElementById('estudiante').hidden=false" >Estudiante <br>
																							<input type="radio" name="tipo" id="radio3" required value="3" onClick="document.getElementById('estudiante').hidden=true"  checked>P&uacute;blico en General<br>
																	               	<?php
																						}
																					?>
																	                <br>

																	                </th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Grado de Instrucción</th>
																				<th>
																					 <?php
																						$estad = $ejecutarconsulta[CodigoGrado];
																						$bestado = mysqli_query ($conecto, "SELECT * FROM grado_instruccion WHERE CodigoGrado = '$estad'");
																					 	$bestado2 = mysqli_query ($conecto, "SELECT * FROM grado_instruccion");
																					 	$restado = mysqli_fetch_array ($bestado);
																					?>
																		        		<select name="grado" class="input2">
																		            	<option value=<?=$estad?>><?=$restado[NombreGrado]?> </option>
																		        	<?php
																						while ($restado2 = mysqli_fetch_array($bestado2)){
																							if ($restado[NombreGrado]!=$restado2[NombreGrado]){
																							echo '<option value='.$restado2[CodigoGrado].'>'.$restado2[NombreGrado].'</option>';}
																						}               
																						?>
																						</select>
																				</th>
																			</tr>
																			<tr class="trestructura1">
																				<th>Institución</th>
																				<th><input type="text" value="<?=$ejecutarconsulta[NombreInstitucion];?>"></th>
																			</tr>
																		</thead>
																		<tbody>
																	        <tr>
																	 			<td data-title="Cédula:"></td>
																			</tr>
																		</tbody>
													            	</table>
													            	</form>
																	</div>				
																</div>
																<!--Footer de la ventana-->
																<div class="modal-footer tamano">
																	<button type="button" class="btn btn-primary">Guardar</button>
																	<button type="button" class="btn btn-defauld" data-dismiss="modal">Cerrar</button>
																</div>
												</div></div></div></div>
											<a onClick="return confirm('¿Desea realmente eliminar al postulante?');" href="?url=administrador&control=cursos&eliminarid=<?=$mostrarcursos[CodigoCursos]?>"><img class="img-responsive img1" alt="Imagen responsive" src="images/eliminar.png" title="Permite eliminar todos los datos del postulante"></a></td>
										<td class="tdestructura" data-title="Postulación:">
											<?php
												$cedu = $mostrarusuario[Id_Personas];
									            $consultarpostulacion = mysqli_query($conecto, "SELECT * FROM realizan_postulacion, cursos WHERE realizan_postulacion.Id_Personas = '$cedu' AND realizan_postulacion.Id_Cursos = cursos.Id_Cursos") or die (mysqli_error($conecto));											 
												if(mysqli_num_rows($consultarpostulacion)==0){
													echo "No se encuentra postulado en ningun curso";
												} 
													echo "".$mostrarusuario['NombreCursos']," ";	 
											?>
										</td>
									</tr>
									<?php }
								echo"<tfoot align='center'>
										<tr class='container'>
											<td colspan='14'>
												<div id='paging'>
													<ul>
														<li><a href='#'><span>Anterior</span></a></li>
														<li><a href='#' class='active'><span>1</span></a></li>
														<li><a href='#'><span>2</span></a></li>
														<li><a href='#'><span>3</span></a></li>
														<li><a href='#'><span>Siguiente</span></a></li>
													</ul>
												</div>
											</td>
										</tr>
									</tfoot>
								</tbody></table></section></div>";
								echo "<strong>Total</strong> ".$cont

					?>

		<!--<div class="container">
			<br>
			<a href="#ventana1" class="btn btn-primary btn-lg" data-toggle="modal">Insertar curso</a>

			<div class="modal fade" id="ventana1">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Header de la ventana -->
						<!--<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-tittle text-center">
								Datos del curso
							</h4>
						</div>

						<!-- Contenido de la ventana-->
						<!--<div class="modal-body">
							<div class="table-responsive">
									<!--Muestra una Tabla de insertar el curso-->
									<!--<table align="center" class="table">
										<thead>
							                <tr>
								 				<td colspan="1" class="td"><strong>Nombre del curso:</strong></td>
												<td colspan="1" class="td"><input pattern="[a-zA-ZÑÍÓÚÉÁ0-9\sñáóíé]{3,30}" name="nombre" required title="El campo debe llenarse con letrasMay&uacute;sculas, min&uacute;sculas o n&uacute;meros, no se aceptan caracteres especiales, ejemplo: Soporte t&eacute;cnico 1" maxlength="30"></td>
											</tr>
									 	</thead>
										<tbody>
						           			<tr>
							                    <td colspan="1" class="td"><strong>Descripci&oacute;n del curso:</strong></td>
							                    <td class="td"><textarea pattern="[a-zñáóíéA-Z0-9\s]{3,300}" name="descripcion" required title="El campo puede llenarse con letras May&uacute;sculas, letras min&uacute;sculas o n&uacute;meros, ejemplo: Configuraci&oacute;n b&aacute;sica de cualquier sistema basado en GNU/Linux"></textarea></td>
						            		</tr>
						            	</tbody>
					            </table>
							</div>				
						</div>
						<!--Footer de la ventana-->
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-primary">Guardar</button>

							<button type="button" class="btn btn-defauld" data-dismiss="modal">Cerrar</button>
							</div>
					</div>
				</div>
			</div>
		</div>

					<nav>
						<div class="center-block">
							<ul class="pagination centrado">
								<li class="disabled"><a href="#">&laquo;<span class="sr-only">Anterior</span></a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&raquo;<span class="sr-only">Siguiente</span></a></li>
							</ul>
						</div>
					</nav>	-->

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>