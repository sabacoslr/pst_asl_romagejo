<!DOCTYPE html>
<html lang="es">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0 ">
		<link rel="stylesheet" href="css/bootstrap.min.css">
		<link rel="stylesheet" href="css/estilos.css">
		<title>Postulantes</title>
	</head>
	<body class="container">
		<?php
			/*Conexión con la base de datos*/
			require_once("./configuracion/conectarse.conf.php");
			/*Diferentes funciones que tiene el sistema*/
			require_once("./configuracion/funciones.conf.php");
			/*Para que la sesión este activada*/
			@session_start();
		?>

		<!-- Banner del sistema-->
		<img class="img-responsive" alt="Imagen responsive" src="images/header1.png">	
		<!--Fin del Banner-->

		<header>
			<nav class="navbar navbar-default" role="navigation">	
			<div class="container">
				<!--Botón que se coloca cuando las pantallas son mas pequeñas-->
				<div class="navbar-header">
					<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navegacion">
						<span class="sr-only">Desplegar / ocultar menu</span>
						<span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
					</button>
				</div>
				<!--Fin del bótón-->

	    		<!-- Inicia Menu-->
	    		<div class="collapse navbar-collapse" id="navegacion">
				    	<ul class="nav navbar-nav">
				        	<li class="active"><a href="#">Inicio</a></li>

				        	<li>
				         		<a href="" class="dropdown-toggle" data-toggle="dropdown">Cursos <span class="caret"></span></a>
								<ul class="dropdown-menu aria-labelledby="dLabel"" role="menu" >
									<li><a href="#">Consultar cursos</a></li>
								</ul>
				         	</li>

				         	<li class="dropdown">
				         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Postulantes <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Visualizar postulantes</a></li>
								</ul>
				         	</li>

				         	<li class="dropdown">
				         		<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Reportes estadísticos <span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Reporte de participante por curso</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Reporte por género</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Reporte por tipo de participante</a></li>
									<li role="separator" class="divider"></li>
								</ul>
				         	</li>

					        <li><a href="#">Ayuda</a></li>

					        <li class="dropdown">
					        	<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button">Configuración
					        	<span class="caret"></span></a>
								<ul class="dropdown-menu" role="menu">
									<li><a href="#">Perfil</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Niveles de usuarios</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Características generales de otras instituciones</a></li>
									<li role="separator" class="divider"></li>
									<li><a href="#">Salir</a></li>
									<li role="separator" class="divider"></li>
								</ul>
					        </li>
					   </ul>
	  			</div>
	  			<!--Fin del menú -->
	  			</div>
				</nav>
		</header>
			
		<?php if($_GET[control]==""){

		}
			/*ADMINISTRADORES*/
			$obtener2=$_GET['control'];
		?>
		<!--Inicia tabla de cursos-->
		<hr>
		<h1 class="h1" align="center">POSTULANTES</h1><br>
		<div class="row">
			<section class="container">
				<form name="filtro1" method="post" style="float:right">
						<p class="p">
						<select name="filtrar1">
							<option> Seleccione el curso </option>
							<?php
								$cfiltrar = mysqli_query($conecto,"SELECT * FROM cursos ORDER by NombreCursos DESC");
								while ($carg = mysqli_fetch_array($cfiltrar)){
							?>
							<option value="<?=$carg[Id_Cursos]?>"><?=$carg[NombreCursos]?></option>
						    <?php }	?>          
    					</select>
						<input type="submit" name="filtrar" value="Consultar"></p>
				</form>
		
				<?php						   
				if(isset($_POST[filtrar])){
					$tipocurso = $_POST[filtrar1];
					$consultarusuario = mysqli_query($conecto, "SELECT * FROM personas, provincia, participante, grado_instruccion, realizan_postulacion, cursos, telefono
					WHERE 
						realizan_postulacion.Id_Cursos = '$tipocurso' AND 
						realizan_postulacion.Id_personas = personas.Id_Personas AND		
						personas.CodigoProvincia = provincia.CodigoProvincia AND 
						personas.CodigoParticipante = participante.CodigoParticipante AND 
						personas.CodigoGrado = grado_instruccion.CodigoGrado AND
						realizan_postulacion.Id_Personas = personas.Id_Personas AND
						realizan_postulacion.Id_Cursos = cursos.Id_Cursos AND
						telefono.Id_Personas = personas.Id_Personas") or die(mysqli_error($conecto));
					$cont =  mysqli_num_rows($consultarusuario);
					
				} else {
					$consultarusuario = mysqli_query($conecto, "SELECT * FROM personas, provincia, participante, grado_instruccion, realizan_postulacion, cursos, telefono  WHERE 
		  		    realizan_postulacion.Id_Personas = personas.Id_Personas AND		
					personas.CodigoProvincia = provincia.CodigoProvincia AND 
					personas.CodigoParticipante = participante.CodigoParticipante AND 
					personas.CodigoGrado = grado_instruccion.CodigoGrado  AND		
					realizan_postulacion.Id_Personas = personas.Id_Personas AND
					realizan_postulacion.Id_Cursos = cursos.Id_Cursos AND
					telefono.Id_Personas = personas.Id_Personas	ORDER BY realizan_postulacion.Id_Postulacion") or die(mysqli_error($conecto));
				}
					
						/*Muestra una Tabla de los cursos que se encuentran en la Base de Datos*/
						echo '<div class="datagrid" id="no-more-tables"> <table class="col-md-12 table-bordered table-responsive table-condensed cf">
								<thead class="cf">
									<tr class="trestructura1"> 
										<th class="tdestructura" align="center">#</th>
										<th class="tdestructura">Cédula</th>
										<th class="tdestructura">Apellido</th>
										<th class="tdestructura">Nombre</th>
										<th class="tdestructura">Género</th>
										<th class="tdestructura">Correo</th>
										<th class="tdestructura">Teléfono</th>
										<th class="tdestructura">Provincia</th>
										<th class="tdestructura">Tipo de Participante</th>
										<th class="tdestructura">Grado de Instrucción</th>
										<th class="tdestructura">Institución</th>
										<th class="tdestructura">Funciones</th>
										<th class="tdestructura">Postulación</th>
									</tr>
								</thead>';
								/*Contador que permite saber la cantidad de cursos que se van a consultar*/
								$num = 0;
								while($mostrarusuario=mysqli_fetch_array($consultarusuario)){
								/*Ejecucion del Contador*/
								$num=$num+1;
					?>
								<tbody>
									<tr> 	
										<td class="tdestructura" data-title="#"><?=$num?></td>
			                            <td class="tdestructura" data-title="Cédula:"><?=$mostrarusuario[CedulaPersonas]?></td>
			                            <td class="tdestructura" data-title="Apellido:"><?=$mostrarusuario[ApellidoPersonas]?></td>
			                            <td class="tdestructura" data-title="Nombre:"><?=$mostrarusuario[NombrePersonas]?></td>	
			                            <td class="tdestructura" data-title="Género:"><?=$mostrarusuario[SexoPersonas]?></td>
			                            <td class="tdestructura" data-title="Correo:"><?=$mostrarusuario[CorreoPersonas]?></td>		
			                            <td class="tdestructura" data-title="Teléfono:"><?=$mostrarusuario[TelefonoPersonas]?></td>			                            	                            
			                            <td class="tdestructura" data-title="Provincia:"><?=$mostrarusuario[NombreEstados]?></td>	
			                            <td class="tdestructura" data-title="Participante:"><?=$mostrarusuario[Participante]?></td>
			                      		<td class="tdestructura" data-title="Instrucción:"><?=$mostrarusuario[NombreGrado]?></td>
			                      		<td class="tdestructura" data-title="Intituciones:"><?=$mostrarusuario[NombreInstitucion]?></td>
			                            
										<td align="center" data-title="Funciones:">
											<a onClick="return confirm('¿Desea realmente modificar al postulante?');"  href="" ><img class="img-responsive img1" alt="Imagen responsive" src="images/editar.png" title="Permite modificar los datos del postulante"></a>
											<a onClick="return confirm('¿Desea realmente eliminar al postulante?');" href=""><img class="img-responsive img1" alt="Imagen responsive" src="images/eliminar.png" title="Permite eliminar todos los datos del postulante"></a></td>
										<td class="tdestructura" data-title="Postulación:">
											<?php
												$cedu = $mostrarusuario[Id_Personas];
									            $consultarpostulacion = mysqli_query($conecto, "SELECT * FROM realizan_postulacion, cursos WHERE realizan_postulacion.Id_Personas = '$cedu' AND realizan_postulacion.Id_Cursos = cursos.Id_Cursos") or die (mysqli_error($conecto));											 
												if(mysqli_num_rows($consultarpostulacion)==0){
													echo "No se encuentra postulado en ningun curso";
												}
													echo "".$mostrarusuario['NombreCursos']," ";	 
											?>
										</td>
									</tr>
									<?php
								echo"<tfoot align='center'>
										<tr class='container'>
											<td colspan='14'>
												<div id='paging'>
													<ul>
														<li><a href='#'><span>Anterior</span></a></li>
														<li><a href='#' class='active'><span>1</span></a></li>
														<li><a href='#'><span>2</span></a></li>
														<li><a href='#'><span>3</span></a></li>
														<li><a href='#'><span>Siguiente</span></a></li>
													</ul>
												</div>
											</td>
										</tr>
									</tfoot>
								</tbody></table></section></div>";
								echo "<strong>Total</strong> ".$cont
					?>

		<!--<div class="container">
			<br>
			<a href="#ventana1" class="btn btn-primary btn-lg" data-toggle="modal">Insertar curso</a>

			<div class="modal fade" id="ventana1">
				<div class="modal-dialog">
					<div class="modal-content">
						<!-- Header de la ventana -->
						<!--<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
							<h4 class="modal-tittle text-center">
								Datos del curso
							</h4>
						</div>

						<!-- Contenido de la ventana-->
						<!--<div class="modal-body">
							<div class="table-responsive">
									<!--Muestra una Tabla de insertar el curso-->
									<!--<table align="center" class="table">
										<thead>
							                <tr>
								 				<td colspan="1" class="td"><strong>Nombre del curso:</strong></td>
												<td colspan="1" class="td"><input pattern="[a-zA-ZÑÍÓÚÉÁ0-9\sñáóíé]{3,30}" name="nombre" required title="El campo debe llenarse con letrasMay&uacute;sculas, min&uacute;sculas o n&uacute;meros, no se aceptan caracteres especiales, ejemplo: Soporte t&eacute;cnico 1" maxlength="30"></td>
											</tr>
									 	</thead>
										<tbody>
						           			<tr>
							                    <td colspan="1" class="td"><strong>Descripci&oacute;n del curso:</strong></td>
							                    <td class="td"><textarea pattern="[a-zñáóíéA-Z0-9\s]{3,300}" name="descripcion" required title="El campo puede llenarse con letras May&uacute;sculas, letras min&uacute;sculas o n&uacute;meros, ejemplo: Configuraci&oacute;n b&aacute;sica de cualquier sistema basado en GNU/Linux"></textarea></td>
						            		</tr>
						            	</tbody>
					            </table>
							</div>				
						</div>
						<!--Footer de la ventana-->
						<!--<div class="modal-footer">
							<button type="button" class="btn btn-primary">Guardar</button>

							<button type="button" class="btn btn-defauld" data-dismiss="modal">Cerrar</button>
							</div>
					</div>
				</div>
			</div>
		</div>

					<nav>
						<div class="center-block">
							<ul class="pagination centrado">
								<li class="disabled"><a href="#">&laquo;<span class="sr-only">Anterior</span></a></li>
								<li class="active"><a href="#">1</a></li>
								<li><a href="#">1</a></li>
								<li><a href="#">2</a></li>
								<li><a href="#">3</a></li>
								<li><a href="#">4</a></li>
								<li><a href="#">5</a></li>
								<li><a href="#">&raquo;<span class="sr-only">Siguiente</span></a></li>
							</ul>
						</div>
					</nav>	-->

		<script src="js/jquery.js"></script>
		<script src="js/bootstrap.min.js"></script>
	</body>
</html>