-- phpMyAdmin SQL Dump
-- version 4.2.7.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 09-09-2015 a las 22:37:54
-- Versión del servidor: 5.6.20
-- Versión de PHP: 5.5.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `sistema_postulacion1`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `cursos`
--

CREATE TABLE IF NOT EXISTS `cursos` (
`Id_Cursos` int(11) NOT NULL,
  `NombreCursos` varchar(255) NOT NULL,
  `DescripcionCursos` text NOT NULL,
  `CodigoEstadoCursos` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=10 ;

--
-- Volcado de datos para la tabla `cursos`
--

INSERT INTO `cursos` (`Id_Cursos`, `NombreCursos`, `DescripcionCursos`, `CodigoEstadoCursos`) VALUES
(1, 'Programación en C++', 'Es un lenguaje de programación diseñado a mediados de los años 1980 por Bjarne Stroustrup. La intención de su creación fue el extender al lenguaje de programación C mecanismos que permiten la manipulación de objetos. En ese sentido, desde el punto de vista de los lenguajes orientados a objetos, el C++ es un lenguaje híbrido. Posteriormente se añadieron facilidades de programación genérica, que se sumaron a los paradigmas de programación estructurada y programación orientada a objetos. Por esto se suele decir que el C++ es un lenguaje de programación multiparadigma. Actualmente existe un estándar, denominado ISO C++, al que se han adherido la mayoría de los fabricantes de compiladores más modernos. Existen también algunos intérpretes, tales como ROOT. Una particularidad del C++ es la posibilidad de redefinir los operadores, y de poder crear nuevos tipos que se comporten como tipos fundamentales.', 1),
(2, 'Base de datos con MySQL', 'Es un sistema de gestión de bases de datos relacional, multihilo y multiusuario con más de seis millones de instalaciones. MySQL AB desde enero de 2008 una subsidiaria de Sun Microsystems y ésta a su vez de Oracle Corporation desde abril de 200 desarrolla MySQL como software libre en un esquema de licenciamiento dual.', 1),
(3, 'Programación en PHP', 'Es un lenguaje de programación de uso general de código del lado del servidor originalmente diseñado para el desarrollo web de contenido dinámico. Fue uno de los primeros lenguajes de programación del lado del servidor que se podían incorporar directamente en el documento HTML en lugar de llamar a un archivo externo que procese los datos. El código es interpretado por un servidor web con un módulo de procesador de PHP que genera la página Web resultante.', 2),
(4, 'Programación en Python', 'Es un lenguaje de programación interpretado cuya filosofía hace hincapié en una sintaxis que favorezca un código legible. Se trata de un lenguaje de programación multiparadigma, ya que soporta orientación a objetos, programación imperativa y, en menor medida, programación funcional. Es un lenguaje interpretado, usa tipado dinámico y es multiplataforma.', 1),
(5, 'Programación en JavaScript', 'Es un lenguaje de programación interpretado, dialecto del estándar ECMAScript. Se define como orientado a objetos, basado en prototipos, imperativo, débilmente tipado y dinámico. Se utiliza principalmente en su forma del lado del cliente (client-side), implementado como parte de un navegador web permitiendo mejoras en la interfaz de usuario y páginas web dinámicas4 aunque existe una forma de JavaScript del lado del servidor (Server-side JavaScript o SSJS). Su uso en aplicaciones externas a la web, por ejemplo en documentos PDF, aplicaciones de escritorio (mayoritariamente widgets) es también significativo. JavaScript se diseñó con una sintaxis similar al C, aunque adopta nombres y convenciones del lenguaje de programación Java. Sin embargo Java y JavaScript no están relacionados y tienen semánticas y propósitos diferentes.', 1),
(6, 'Base de datos con PostgreSQL', 'Es un Sistema de gestión de bases de datos relacional orientado a objetos y libre, publicado bajo la licencia BSD. Como muchos otros proyectos de código abierto, el desarrollo de PostgreSQL no es manejado por una empresa y/o persona, sino que es dirigido por una comunidad de desarrolladores que trabajan de forma desinteresada, altruista, libre y/o apoyados por organizaciones comerciales. Dicha comunidad es denominada el PGDG (PostgreSQL Global Development Group).', 1),
(7, 'Edición de imágenes con GIMP', 'Es un programa de edición de imágenes digitales en forma de mapa de bits, tanto dibujos como fotografías. Es un programa libre y gratuito. Forma parte del proyecto GNU y está disponible bajo la Licencia pública general de GNU y GNU Lesser General Public License GIMP tiene herramientas que se utilizan para el retoque y edición de imágenes, dibujo de formas libres, cambiar el tamaño, recortar, hacer fotomontajes, convertir a diferentes formatos de imagen, y otras tareas más especializadas. Se pueden también crear imágenes animadas en formato GIF e imágenes animadas en formato MPEG usando un plugin de animación.', 1),
(8, 'XHTML/ XML/ CSS', 'XHTML  es básicamente HTML expresado como XML válido. Es más estricto a nivel técnico, pero esto permite que posteriormente sea más fácil al hacer cambios o buscar errores entre otros. En su versión 1.0, XHTML es solamente la versión XML de HTML, por lo que tiene, básicamente, las mismas funcionalidades, pero cumple las especificaciones, más estrictas, de XML. Su objetivo es avanzar en el proyecto del World Wide Web Consortium de lograr una web semántica, donde la información, y la forma de presentarla estén claramente separadas. \r\nXML es un lenguaje de marcas desarrollado por el World Wide Web Consortium (W3C) utilizado para desalmacenar datos en forma legible. Deriva del lenguaje SGML y permite definir la gramática de lenguajes específicos (de la misma manera que HTML es a su vez un lenguaje definido por SGML) para estructurar documentos grandes. A diferencia de otros lenguajes, XML da soporte a bases de datos, siendo útil cuando varias aplicaciones deben comunicarse entre sí o integrar información.\r\nCss  es un lenguaje que define la apariencia de un documento escrito en un lenguaje de marcado (por ejemplo, HTML). Así, a los elementos de la página web creados con HTML se les dará la apariencia que se desee utilizando CSS: colores, espacios entre elementos, tipos de letra, separando de esta forma la estructura de la presentación. Esta separación entre la estructura y la presentación es muy importante, ya que permite que sólo cambiando los CSS se modifique completamente el aspecto de una página web. Esto posibilita, entre otras cosas, que los usuarios puedan usar hojas de estilo personalizadas (como hojas de estilo de alto contraste o de accesibilidad).', 1),
(9, 'Ofimática Avanzada', 'Designa al conjunto de técnicas, aplicaciones y herramientas informáticas que se utilizan en funciones de oficina para optimizar, automatizar, y mejorar tareas y procedimientos relacionados. Las herramientas ofimáticas permiten idear, crear, manipular, transmitir o almacenar la información necesaria en una oficina. Actualmente es fundamental que las oficinas estén conectadas a una red local o a Internet.', 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `estado_cursos`
--

CREATE TABLE IF NOT EXISTS `estado_cursos` (
`CodigoEstadoCursos` int(1) NOT NULL,
  `EstadoCursos` char(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `estado_cursos`
--

INSERT INTO `estado_cursos` (`CodigoEstadoCursos`, `EstadoCursos`) VALUES
(1, 'Activado'),
(2, 'Desactivado');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `grado_instruccion`
--

CREATE TABLE IF NOT EXISTS `grado_instruccion` (
  `CodigoGrado` int(1) NOT NULL,
  `NombreGrado` char(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `grado_instruccion`
--

INSERT INTO `grado_instruccion` (`CodigoGrado`, `NombreGrado`) VALUES
(1, 'Básica'),
(2, 'Secundaria'),
(3, 'Universitario'),
(4, 'Técnico Medio Universitario'),
(5, 'Maestría'),
(6, 'Doctorado'),
(7, 'Otro');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `historial`
--

CREATE TABLE IF NOT EXISTS `historial` (
`Id_Postulacion` int(11) NOT NULL,
  `Id_Personas` int(11) NOT NULL,
  `Id_Cursos` int(11) NOT NULL,
  `FechaPostulacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `historial`
--

INSERT INTO `historial` (`Id_Postulacion`, `Id_Personas`, `Id_Cursos`, `FechaPostulacion`) VALUES
(1, 1, 1, '2015-08-16 22:09:44'),
(2, 1, 3, '2015-09-08 19:34:17'),
(3, 1, 3, '2015-09-08 19:34:24');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `nivel_usuario`
--

CREATE TABLE IF NOT EXISTS `nivel_usuario` (
  `NivelUsuario` int(1) NOT NULL,
  `NombreNivelUsuario` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `nivel_usuario`
--

INSERT INTO `nivel_usuario` (`NivelUsuario`, `NombreNivelUsuario`) VALUES
(0, 'Usuario'),
(1, 'Administrador'),
(2, 'Súper Administrador');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `participante`
--

CREATE TABLE IF NOT EXISTS `participante` (
`CodigoParticipante` int(1) NOT NULL,
  `Participante` varchar(20) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `participante`
--

INSERT INTO `participante` (`CodigoParticipante`, `Participante`) VALUES
(1, 'Funcionario Público'),
(2, 'Estudiante'),
(3, 'Público en general');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `personas`
--

CREATE TABLE IF NOT EXISTS `personas` (
`Id_Personas` int(11) NOT NULL COMMENT 'El la llave primaria que entrelazan las tablas del sistema',
  `CedulaPersonas` varchar(10) NOT NULL,
  `ApellidoPersonas` char(20) NOT NULL,
  `NombrePersonas` char(20) NOT NULL,
  `SexoPersonas` char(1) NOT NULL,
  `CorreoPersonas` varchar(150) NOT NULL,
  `NombreInstitucion` varchar(150) NOT NULL,
  `CodigoProvincia` int(2) NOT NULL,
  `CodigoParticipante` int(1) NOT NULL,
  `CodigoGrado` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `personas`
--

INSERT INTO `personas` (`Id_Personas`, `CedulaPersonas`, `ApellidoPersonas`, `NombrePersonas`, `SexoPersonas`, `CorreoPersonas`, `NombreInstitucion`, `CodigoProvincia`, `CodigoParticipante`, `CodigoGrado`) VALUES
(1, '22665400', 'Castillo', 'Rosmary', 'f', 'carrillorosmary@gmail.com', 'UPTM', 14, 2, 4);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `proponer_cursos`
--

CREATE TABLE IF NOT EXISTS `proponer_cursos` (
`Id_Propuesta` int(11) NOT NULL COMMENT 'Es la clave primaria de la tabla propuesta',
  `Id_Personas` int(11) NOT NULL,
  `Propuesta` varchar(255) DEFAULT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Volcado de datos para la tabla `proponer_cursos`
--

INSERT INTO `proponer_cursos` (`Id_Propuesta`, `Id_Personas`, `Propuesta`) VALUES
(1, 1, 'Respoteria'),
(2, 1, 'Maquillaje de payasos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `provincia`
--

CREATE TABLE IF NOT EXISTS `provincia` (
  `CodigoProvincia` int(2) NOT NULL,
  `NombreEstados` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `provincia`
--

INSERT INTO `provincia` (`CodigoProvincia`, `NombreEstados`) VALUES
(1, 'Apure'),
(2, 'Amazonas'),
(3, 'Anzoategui'),
(4, 'Aragua'),
(5, 'Barinas'),
(6, 'Bolívar'),
(7, 'Carabobo'),
(8, 'Cojedes'),
(9, 'Delta Amacuro'),
(10, 'Distrito Capital'),
(11, 'Falcón'),
(12, 'Guárico'),
(13, 'Lara'),
(14, 'Mérida'),
(15, 'Miranda'),
(16, 'Monagas'),
(17, 'Nueva Esparta'),
(18, 'Portuguesa'),
(19, 'Sucre'),
(20, 'Táchira'),
(21, 'Trujillo'),
(22, 'Valencia'),
(23, 'Vargas'),
(24, 'Zulia');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `realizan_postulacion`
--

CREATE TABLE IF NOT EXISTS `realizan_postulacion` (
`Id_Postulacion` int(11) NOT NULL,
  `Id_Personas` int(11) NOT NULL,
  `Id_Cursos` int(11) NOT NULL,
  `FechaPostulacion` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `realizan_postulacion`
--

INSERT INTO `realizan_postulacion` (`Id_Postulacion`, `Id_Personas`, `Id_Cursos`, `FechaPostulacion`) VALUES
(1, 1, 3, '2015-09-08 19:34:17');

--
-- Disparadores `realizan_postulacion`
--
DELIMITER //
CREATE TRIGGER `Actualizar_Registro` AFTER INSERT ON `realizan_postulacion`
 FOR EACH ROW INSERT INTO historial (Id_Postulacion,Id_Personas,Id_Cursos,FechaPostulacion) VALUES(NEW.Id_Postulacion,NEW.Id_Personas,NEW.Id_Cursos,NEW.FechaPostulacion)
//
DELIMITER ;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `telefono`
--

CREATE TABLE IF NOT EXISTS `telefono` (
  `TelefonoPersonas` varchar(12) NOT NULL,
  `CodigoTelefonos` int(1) NOT NULL,
  `Id_Personas` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `telefono`
--

INSERT INTO `telefono` (`TelefonoPersonas`, `CodigoTelefonos`, `Id_Personas`) VALUES
('04149785571', 1, 1);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tipostelefonos`
--

CREATE TABLE IF NOT EXISTS `tipostelefonos` (
`CodigoTelefono` int(1) NOT NULL,
  `TiposTelefonos` varchar(12) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=4 ;

--
-- Volcado de datos para la tabla `tipostelefonos`
--

INSERT INTO `tipostelefonos` (`CodigoTelefono`, `TiposTelefonos`) VALUES
(1, 'Celular'),
(2, 'Local'),
(3, 'Trabajo');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`Id_Usuario` int(11) NOT NULL,
  `NombreUsuario` varchar(32) NOT NULL,
  `ContrasenaUsuario` varchar(32) NOT NULL,
  `PreguntaUsuario` varchar(100) NOT NULL,
  `RespuestaUsuario` varchar(100) NOT NULL,
  `NivelUsuario` int(1) NOT NULL,
  `Id_Personas` int(11) NOT NULL,
  `CodigoEstadoCursos` int(1) NOT NULL
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`Id_Usuario`, `NombreUsuario`, `ContrasenaUsuario`, `PreguntaUsuario`, `RespuestaUsuario`, `NivelUsuario`, `Id_Personas`, `CodigoEstadoCursos`) VALUES
(1, 'Rosmary', 'e396bbb053529d2ddb17b100aa04d7c5', 'Nombre de mi mama', 'Marylin', 2, 1, 1);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `cursos`
--
ALTER TABLE `cursos`
 ADD PRIMARY KEY (`Id_Cursos`), ADD KEY `CodigoEstadoCursos` (`CodigoEstadoCursos`);

--
-- Indices de la tabla `estado_cursos`
--
ALTER TABLE `estado_cursos`
 ADD PRIMARY KEY (`CodigoEstadoCursos`);

--
-- Indices de la tabla `grado_instruccion`
--
ALTER TABLE `grado_instruccion`
 ADD PRIMARY KEY (`CodigoGrado`);

--
-- Indices de la tabla `historial`
--
ALTER TABLE `historial`
 ADD PRIMARY KEY (`Id_Postulacion`), ADD KEY `Id_Personas` (`Id_Personas`,`Id_Cursos`), ADD KEY `Id_Cursos` (`Id_Cursos`);

--
-- Indices de la tabla `nivel_usuario`
--
ALTER TABLE `nivel_usuario`
 ADD PRIMARY KEY (`NivelUsuario`);

--
-- Indices de la tabla `participante`
--
ALTER TABLE `participante`
 ADD PRIMARY KEY (`CodigoParticipante`);

--
-- Indices de la tabla `personas`
--
ALTER TABLE `personas`
 ADD PRIMARY KEY (`Id_Personas`), ADD UNIQUE KEY `CedulaPersonas` (`CedulaPersonas`), ADD KEY `CodigoProvincia` (`CodigoProvincia`), ADD KEY `CodigoParticipante` (`CodigoParticipante`), ADD KEY `CodigoGrado` (`CodigoGrado`), ADD KEY `CodigoGrado_2` (`CodigoGrado`);

--
-- Indices de la tabla `proponer_cursos`
--
ALTER TABLE `proponer_cursos`
 ADD PRIMARY KEY (`Id_Propuesta`), ADD KEY `Id_Personas` (`Id_Personas`);

--
-- Indices de la tabla `provincia`
--
ALTER TABLE `provincia`
 ADD PRIMARY KEY (`CodigoProvincia`);

--
-- Indices de la tabla `realizan_postulacion`
--
ALTER TABLE `realizan_postulacion`
 ADD PRIMARY KEY (`Id_Postulacion`), ADD KEY `Id_Personas` (`Id_Personas`), ADD KEY `Id_Cursos` (`Id_Cursos`);

--
-- Indices de la tabla `telefono`
--
ALTER TABLE `telefono`
 ADD PRIMARY KEY (`TelefonoPersonas`), ADD KEY `CodigoTelefonos` (`CodigoTelefonos`,`Id_Personas`), ADD KEY `Id_Personas` (`Id_Personas`);

--
-- Indices de la tabla `tipostelefonos`
--
ALTER TABLE `tipostelefonos`
 ADD PRIMARY KEY (`CodigoTelefono`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`Id_Usuario`), ADD KEY `NivelUsuario` (`NivelUsuario`,`Id_Personas`,`CodigoEstadoCursos`), ADD KEY `Id_Personas` (`Id_Personas`), ADD KEY `CodigoEstadoCursos` (`CodigoEstadoCursos`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `cursos`
--
ALTER TABLE `cursos`
MODIFY `Id_Cursos` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `estado_cursos`
--
ALTER TABLE `estado_cursos`
MODIFY `CodigoEstadoCursos` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `historial`
--
ALTER TABLE `historial`
MODIFY `Id_Postulacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `participante`
--
ALTER TABLE `participante`
MODIFY `CodigoParticipante` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `personas`
--
ALTER TABLE `personas`
MODIFY `Id_Personas` int(11) NOT NULL AUTO_INCREMENT COMMENT 'El la llave primaria que entrelazan las tablas del sistema',AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `proponer_cursos`
--
ALTER TABLE `proponer_cursos`
MODIFY `Id_Propuesta` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Es la clave primaria de la tabla propuesta',AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT de la tabla `realizan_postulacion`
--
ALTER TABLE `realizan_postulacion`
MODIFY `Id_Postulacion` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT de la tabla `tipostelefonos`
--
ALTER TABLE `tipostelefonos`
MODIFY `CodigoTelefono` int(1) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `Id_Usuario` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `cursos`
--
ALTER TABLE `cursos`
ADD CONSTRAINT `cursos_ibfk_1` FOREIGN KEY (`CodigoEstadoCursos`) REFERENCES `estado_cursos` (`CodigoEstadoCursos`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `personas`
--
ALTER TABLE `personas`
ADD CONSTRAINT `personas_ibfk_1` FOREIGN KEY (`CodigoGrado`) REFERENCES `grado_instruccion` (`CodigoGrado`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `personas_ibfk_2` FOREIGN KEY (`CodigoProvincia`) REFERENCES `provincia` (`CodigoProvincia`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `personas_ibfk_3` FOREIGN KEY (`CodigoParticipante`) REFERENCES `participante` (`CodigoParticipante`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `proponer_cursos`
--
ALTER TABLE `proponer_cursos`
ADD CONSTRAINT `proponer_cursos_ibfk_1` FOREIGN KEY (`Id_Personas`) REFERENCES `personas` (`Id_Personas`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `realizan_postulacion`
--
ALTER TABLE `realizan_postulacion`
ADD CONSTRAINT `realizan_postulacion_ibfk_1` FOREIGN KEY (`Id_Personas`) REFERENCES `personas` (`Id_Personas`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `realizan_postulacion_ibfk_2` FOREIGN KEY (`Id_Cursos`) REFERENCES `cursos` (`Id_Cursos`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `telefono`
--
ALTER TABLE `telefono`
ADD CONSTRAINT `telefono_ibfk_1` FOREIGN KEY (`Id_Personas`) REFERENCES `personas` (`Id_Personas`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `telefono_ibfk_2` FOREIGN KEY (`CodigoTelefonos`) REFERENCES `tipostelefonos` (`CodigoTelefono`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Filtros para la tabla `usuarios`
--
ALTER TABLE `usuarios`
ADD CONSTRAINT `usuarios_ibfk_1` FOREIGN KEY (`NivelUsuario`) REFERENCES `nivel_usuario` (`NivelUsuario`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `usuarios_ibfk_2` FOREIGN KEY (`Id_Personas`) REFERENCES `personas` (`Id_Personas`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `usuarios_ibfk_3` FOREIGN KEY (`CodigoEstadoCursos`) REFERENCES `estado_cursos` (`CodigoEstadoCursos`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
